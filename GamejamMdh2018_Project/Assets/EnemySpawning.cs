﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawning : MonoBehaviour
{
    public const float MaxWeightGameTime = 180; //in seconds
    public const int MaxEnemysInPlay = 8;

    public FantasyEnemy[] EnemyPrefabs;
    public SpawnInfo[] SpawnWaves;

    public Vector2 SpawntimerRange = new Vector2(60,10);
    public float SpawnTimerMaxRandom = 5;

    [Range(0,1)]
    public float InsanityInfluence = 1;
    [Range(0, 1)]
    public float GametimeInfluence = 1;
    [Range(0, 1)]
    public float EnemyCountInfluence = 1;

    private List<FantasyEnemy> _enemysInPlay = new List<FantasyEnemy>();
    public int EnemyCount => _enemysInPlay.Count;

    private Player _player;
    private float _gameTime;


    private float _spawnTimer;
    private float _nextSpawnTime = 20;

    protected virtual void Awake()
    {
        _player = FindObjectOfType<Player>();
    }

    protected virtual void Update()
    {
        if(!SpawnPossible())
            return;

        _gameTime += Time.deltaTime;
        _spawnTimer += Time.deltaTime;

        if (_spawnTimer >= _nextSpawnTime)
        {
            SpawnWave();

            _nextSpawnTime = GetNextSpawnTime();
            _spawnTimer = 0;
        }
    }


    public void SpawnWave()
    {
        float weightMax = 0;
        foreach (SpawnInfo wave in SpawnWaves)
        {
            weightMax += GetCurrentWeight(wave);
        }

        float randomPointer = Random.Range(0, weightMax);
        float weightSum = 0;
        SpawnInfo foundWave = null;
        foreach (SpawnInfo wave in SpawnWaves)
        {
            weightSum += GetCurrentWeight(wave);
            if (weightSum >= randomPointer)
            {
                foundWave = wave;
                break;
            }
        }

        if (foundWave == null)
        {
            Debug.LogWarning("No Wave to spawn found!");
            return;
        }

        //Spawn wave
        foreach (int spawnIndex in foundWave.SpawnObjIndecies)
        {
            FantasyEnemy prefabEnemy = EnemyPrefabs[spawnIndex];

            //TODO Spawn by navmesh-point
            Vector2 spawnPos = _player.transform.position + Quaternion.Euler(0, 0, Random.Range(0, 360f)) * Vector2.up * Random.Range(3,6);

            FantasyEnemy spawnedEnemy = Instantiate(prefabEnemy, spawnPos, Quaternion.Euler(90,0,0));
            _enemysInPlay.Add(spawnedEnemy);

            spawnedEnemy.OnDied += RemoveEnemyFromList;
        }
    }

    private void RemoveEnemyFromList(FantasyEnemy enemy)
    {
        enemy.OnDied -= RemoveEnemyFromList;

        _enemysInPlay.Remove(enemy);
    }

    private float GetCurrentWeight(SpawnInfo info)
    {
        if (_gameTime < info.GamestartDelay)
            return 0;
        return Mathf.Lerp(info.MinWeight, info.MaxWeight, Mathf.Clamp01(_gameTime / 180)); //TODO Use GameStartDelay
    }

    private float GetNextSpawnTime()
    {
        return Mathf.Lerp(SpawntimerRange.x, SpawntimerRange.y, GetNextWaveLerpAlpha()) +
               Random.Range(-SpawnTimerMaxRandom, SpawnTimerMaxRandom);
    }

    private float GetNextWaveLerpAlpha()
    {
        float insanityInfluance = _player.Insanity * InsanityInfluence;
        float timeInPlayInfluance = Mathf.Clamp01(_gameTime / MaxWeightGameTime) * GametimeInfluence;
        float enemysInPlayInfluance = Mathf.Clamp01((float) EnemyCount / MaxEnemysInPlay) * EnemyCountInfluence;

        return (insanityInfluance + timeInPlayInfluance + enemysInPlayInfluance) / 3;
    }

    private bool SpawnPossible()
    {
        return EnemyCount < MaxEnemysInPlay;
    }


    [Serializable]
    public class SpawnInfo
    {
        public int[] SpawnObjIndecies;
        public float MinWeight = 50;
        public float MaxWeight = 100;
        public float GamestartDelay;
    }
}
