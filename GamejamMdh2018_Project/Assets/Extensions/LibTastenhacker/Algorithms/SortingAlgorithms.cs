﻿using System;
using System.Collections.Generic;

namespace LibTastenhacker.Algorithms
{
    public static class SortingAlgorithms
    {
        public static void Dummy()
        {
            int[] zahlen = { 6, 3, 4, 5, 8 };
            zahlen.BubbleSort();
        }

        public static void BubbleSort<T>(this IList<T> input) where T : IComparable
        {
            for (int j = 0; j < input.Count; j++)
            {
                bool hasChanged = false;
                for (int i = 0; i < input.Count - 1 - j; i++)
                {
                    if (input[i].CompareTo(input[i + 1]) > 0)
                    {
                        Swap(input, i, i + 1);
                        hasChanged = true;
                    }
                }
                if (!hasChanged)
                    return;
            }
        }

        public static void SelectionSort<T>(this IList<T> input) where T : IComparable
        {
            for (int j = 0; j < input.Count - 1; j++)
            {
                int iMin = j;
                int i;
                for (i = j + 1; i < input.Count; i++)
                {
                    if (input[i].CompareTo(input[iMin]) < 0)
                    {
                        iMin = i;
                    }
                }

                if (iMin != j)
                {
                    Swap(input, j, iMin);
                }
            }
        }

        public static void InsertionSort<T>(this IList<T> input) where T : IComparable
        {
            for (int counter = 0; counter < input.Count - 1; counter++)
            {
                var index = counter + 1;
                while (index > 0)
                {
                    if (input[index - 1].CompareTo(input[index]) > 0)
                    {
                        Swap(input, index - 1, index);
                    }
                    index--;
                }
            }
        }

        public static void QuickSort<T>(this IList<T> input) where T : IComparable
        {
            Quicksort(input, 0, input.Count - 1);
        }

        private static void Quicksort<T>(IList<T> input, int left, int right) where T : IComparable
        {
            if (left < right)
            {
                int teiler = Partition(input, left, right);
                Quicksort(input, left, teiler - 1);
                Quicksort(input, teiler + 1, right);
            }
        }

        private static int Partition<T>(IList<T> input, int left, int right) where T : IComparable
        {
            int i = left;
            int j = right - 1;
            T pivot = input[right];

            do
            {
                while (input[i].CompareTo(pivot) <= 0 && i < right)
                    i += 1;

                while (input[j].CompareTo(pivot) >= 0 && j > left)
                    j -= 1;

                if (i < j)
                {
                    Swap(input, i, j);
                }
            } while (i < j);

            if (input[i].CompareTo(pivot) > 0)
            {
                Swap(input, i, right);
            }
            return i;
        }

        public static void HeapSort<T>(this IList<T> input) where T : IComparable
        {
            //Build-Max-Heap
            int heapSize = input.Count;
            for (int p = (heapSize - 1) / 2; p >= 0; p--)
                MaxHeapify(input, heapSize, p);

            for (int i = input.Count - 1; i > 0; i--)
            {
                Swap(input, i, 0);
                heapSize--;
                MaxHeapify(input, heapSize, 0);
            }
        }

        private static void MaxHeapify<T>(IList<T> input, int heapSize, int index) where T : IComparable
        {
            int left = (index + 1) * 2 - 1;
            int right = (index + 1) * 2;
            int largest;

            if (left < heapSize && input[left].CompareTo(input[index]) > 0)
                largest = left;
            else
                largest = index;

            if (right < heapSize && input[right].CompareTo(input[largest]) > 0)
                largest = right;

            if (largest != index)
            {
                Swap(input, index, largest);
                MaxHeapify(input, heapSize, largest);
            }
        }

        public static void Swap<T>(this IList<T> input, int indexA, int indexB)
        {
            T temp = input[indexA];
            input[indexA] = input[indexB];
            input[indexB] = temp;
        }
    }
}