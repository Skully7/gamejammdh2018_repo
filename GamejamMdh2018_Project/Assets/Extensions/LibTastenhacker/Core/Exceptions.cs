﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//         © 2013-2015 Thomas Hummes
//----------------------------------------------

using System;

namespace Tastenhacker.Core
{
    public class ListEmptyException : Exception { }
}