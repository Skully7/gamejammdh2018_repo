﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

namespace Tastenhacker.Core
{
    public interface IGettable
    {
        T GetValue<T>(string key);

        string GetString(object key);

        int GetInteger(string key);

        bool GetBool(string key);

        float GetFloat(string key);
    }
}