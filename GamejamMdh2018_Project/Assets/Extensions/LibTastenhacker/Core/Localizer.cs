//----------------------------------------------
//          ~ Tastenhacker Library ~
//            � 2013 Thomas Hummes
//----------------------------------------------
#if SIMPLE_SQL
using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using Tastenhacker.Helper;
using UnityEngine;

namespace Tastenhacker.Core
{
    /// <summary>
    /// List Lokalisierungseintr�ge aus einer SQLite-Datenbank aus
    /// </summary>
    /// <remarks>Ben�tigt SQLiteKit</remarks>
    public class Localizer : IGettable
    {
        // erster key = Identifier
        // zweiter key = Sprachk�rzel
        private readonly Dictionary<string, Dictionary<string, string>> _items;
        private readonly string _tableName = "Settings";
        private readonly SQLite.Database _db;
        
        public string CurrentLanguage = "deu";

        /// <summary>
        /// Erzeugt ein neues Lokalisierungsobjekt
        /// </summary>
        /// <param name="db">Datenbank die genutzt werden soll</param>
        /// <param name="tableName">Tabelle, aus der die Lokalisierungseintr�ge ausgelesen werden</param>
        public Localizer(SQLite.Database db, string tableName)
        {
            _db = db;
            _items = new Dictionary<string, Dictionary<string, string>>();
            _tableName = tableName;
            Load();
        }

        public int Count { get { return _items.Count; } }

        /// <summary>
        /// Liest einmalig alle Werte aus der Datenbank aus
        /// </summary>
        private void Load()
        {
            Table tbl = _db.QueryGeneric("SELECT * FROM " + _tableName);
            _items.Clear();

            foreach(Row row in tbl)
            { 
                Dictionary<string, string> values = new Dictionary<string, string>();

                foreach (String column in tbl.Columns.Where(p => (p.Name != "Identifier") && (p.Name != "Description")).Select(p => p.Name))
                {
                    values.Add(column, row.Get<string>(column));
                }
                _items.Add(row.Get<string>("Identifier"), values);
            }
            Debug.Log("Loaded " + _items.Count + " localizations from " + _tableName);
        }

        /// <summary>
        /// Interne Hilfsfunktion, die pr�ft, ob ein Schl�ssel hinterlegt ist
        /// </summary>
        /// <param name="key"></param>
        private void AssertKeyExists(string key)
        {
            if (key == null)
                throw new ArgumentNullException("key");
            if (!_items.ContainsKey(key))
                throw new Exception("Key " + key + " not found");
            if (!_items[key].ContainsKey(CurrentLanguage))
                throw new KeyNotExistsException(CurrentLanguage);
        }

        /// <summary>
        /// Liest einen lokalisierten Wert als beliebigen Typ aus
        /// </summary>
        /// <typeparam name="T">Der Datentyp, in den der Wert konvertiert werden soll (ausgehend von String). 
        /// Nur einsetzbar f�r Typen, die IConvertible implementieren</typeparam>
        /// <param name="key">Die Kennung des zu ermittelnden Wertes</param>
        /// <returns>Den eingelesen und konvertieren Wert</returns>
        /// <exception cref="ArgumentNullException">Wenn der Schl�ssel leer ist</exception>
        /// <exception cref="KeyNotExistsException">Wenn der Schl�ssel in der Datenbank nicht enthalten ist</exception>
        public T GetValue<T>(string key)
        {
            AssertKeyExists(key);
            return (T) Convert.ChangeType(_items[key][CurrentLanguage], typeof (T));
        }

        /// <summary>
        /// Liest einen lokalisierten Wert als String aus
        /// </summary>
        /// <see cref="GetValue{T}"/>
        public string GetString(object key)
        {
            string s = (string) key;
            AssertKeyExists(s);
            return _items[s][CurrentLanguage];
        }

        /// <summary>
        /// Liest einen lokalisierten Wert als Integer aus
        /// </summary>
        /// <see cref="GetValue{T}"/>
        public int GetInteger(string key)
        {
            AssertKeyExists(key);
            return Convert.ToInt32(_items[key][CurrentLanguage]);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool GetBool(string key)
        {
            AssertKeyExists(key);
            return Convert.ToBoolean(_items[key][CurrentLanguage]);
        }

        public float GetFloat(string key)
        {
            AssertKeyExists(key);
            return (float) Convert.ToDouble(_items[key][CurrentLanguage]);
        }
    }
}
#endif