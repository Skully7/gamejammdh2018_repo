﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;

namespace Tastenhacker.Core.Manager
{
    public class Coroutiner : StaticManagerModule<Coroutiner>
    {
        public Coroutine Run(IEnumerator iterationResult)
        {
            CoroutinerInstance routeneHandler = gameObject.AddComponent(typeof (CoroutinerInstance)) as CoroutinerInstance;
            return routeneHandler.ProcessWork(iterationResult);
        }
    }

    public class CoroutinerInstance : MonoBehaviour
    {
        public Coroutine ProcessWork(IEnumerator iterationResult)
        {
            return StartCoroutine(DestroyWhenComplete(iterationResult));
        }

        public IEnumerator DestroyWhenComplete(IEnumerator iterationResult)
        {
            yield return StartCoroutine(iterationResult);
            Destroy(this);
        }
    }

    public class CoroutineSequencer
    {
        readonly List<bool> _subTasks = new List<bool>();

        private readonly MonoBehaviour _owningComponent;

        public CoroutineSequencer(MonoBehaviour owningComponent)
        {
            _owningComponent = owningComponent;
        }

        public void StartSubtask(IEnumerator routine)
        {
            _subTasks.Add(false);
            _owningComponent.StartCoroutine(StartJoinableCoroutine(_subTasks.Count - 1, routine));
        }

        public Coroutine WaitForAll()
        {
            return _owningComponent.StartCoroutine(WaitForAllSubtasks());
        }

        private IEnumerator WaitForAllSubtasks()
        {
            while (true)
            {
                if (_subTasks.All(t => t))
                {
                    break;
                }
                yield return null;
            }
        }

        private IEnumerator StartJoinableCoroutine(int index, IEnumerator coroutine)
        {
            yield return _owningComponent.StartCoroutine(coroutine);
            _subTasks[index] = true;
        }
    }
}