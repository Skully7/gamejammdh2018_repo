﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tastenhacker.Core.Manager
{

    public class Executor : StaticManagerModule<Executor>
    {
        private class ExecutorTask
        {
            public bool Done;

            protected readonly Action _action;

            protected readonly float _executeTime;

            public ExecutorTask(Action action, float delay = 0)
            {
                Done = false;
                _action = action;
                _executeTime = Time.realtimeSinceStartup + delay;
            }

            public virtual bool Check()
            {
                return _executeTime <= Time.realtimeSinceStartup;
            }

            public virtual void Execute()
            {
                _action.Invoke();
                Done = true;
            }
        }

        private class EndOfFramesExecutorTask : ExecutorTask
        {
            private int _framesToSkip;
            private int _skippedFrames;

            public EndOfFramesExecutorTask(Action action, int framesToSkip) : base(action, 0)
            {
                _framesToSkip = framesToSkip;
                _skippedFrames = 0;
            }

            private IEnumerator WaitUntilEndOfFrame()
            {
                while (_skippedFrames < _framesToSkip)
                {
                    yield return new WaitForEndOfFrame();
                    _skippedFrames++;
                }
                _action.Invoke();
                Done = true;
            }

            public override void Execute()
            {
                Manager.Instance.Get<Coroutiner>().Run(WaitUntilEndOfFrame());
            }
        }

        private class ConditionalExecutorTask : ExecutorTask
        {
            private readonly Func<bool> _condition;

            public override bool Check()
            {
                return _condition.Invoke();
            }

            public ConditionalExecutorTask(Action action, Func<bool> condition) : base(action, 0)
            {
                _condition = condition;
            }
        }

        private readonly List<ExecutorTask> _tasks = new List<ExecutorTask>();

        public void ExecDelayed(Action action, float delay)
        {
            _tasks.Add(new ExecutorTask(action, delay));
        }

        public void ExecWhen(Action action, Func<bool> condition)
        {
            _tasks.Add(new ConditionalExecutorTask(action, condition));
        }

        public void ExecOnEndOfFrame(Action action, int framesToSkip)
        {
            _tasks.Add(new EndOfFramesExecutorTask(action, framesToSkip));
        }

        private void Update()
        {
            List<ExecutorTask> tasks = _tasks.Where(t => t.Check()).Reverse().ToList();
            for (int index = tasks.Count - 1; index >= 0; index--)
            {
                ExecutorTask task = tasks[index];
                task.Execute();
            }
            _tasks.RemoveAll(t => t.Done);
        }
    }
}