﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

#if SIMPLE_SQL
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using SQLite;
using Tastenhacker.Helper;

namespace Tastenhacker.Core
{
    public class KeyNotExistsException : Exception
    {
        public KeyNotExistsException(string key) : base(key) { }
    };

    /// <summary>
    /// Class to read & save generic values with a SQLite database
    /// </summary>
    /// <remarks>Relies on SQLiteKit</remarks>
    public class Settings : IGettable, IEnumerable
    {
        private readonly Dictionary<string, string> _items;
        private readonly string _tableName = "Settings";
        private readonly SQLite.Database _db;

        public string FilterColumn;
        public Object FilterValue;

        /// <summary>
        /// Konstruktor. Verbindet automatisch mit der Datenbank und lädt die Werte
        /// </summary>
        /// <param name="db">Referenz auf die zu nutzende ItemDatabase</param>
        /// <param name="tableName">Die zu nutzende Tabelle (muss zwei Spalten "Key" und "Value" haben, beide Strings</param>
        public Settings(SQLite.Database db, string tableName, string filterName = "", Object filterValue = null)
        {
            _items = new Dictionary<string, string>();
            _tableName = tableName;
            _db = db;
            FilterColumn = filterName;
            FilterValue = filterValue;
            Load();
        }

        /// <summary>
        /// Loads all values from the database
        /// </summary>
        private void Load()
        {
            string SQL = "SELECT Key, Value FROM " + _tableName;
            if (FilterColumn != "")
            {
                SQL += " WHERE " + FilterColumn + " = \"" + FilterValue + "\"";
            }

            Table tbl = _db.QueryGeneric(SQL);

            _items.Clear();
            foreach(Row row in tbl)
            {
                _items.Add(row.Get<string>("Key"), row.Get<string>("Value"));
            }
        }

        /// <summary>
        /// Saves all current values in the database
        /// </summary>
        /// <param name="key"></param>
        public void Save(string key)
        {
            string SQL = "SELECT COUNT(*) Anzahl FROM " + _tableName + " WHERE Key = ? ";
            if (FilterColumn.Length > 0)
            {
                SQL += " AND " + FilterColumn + " = \"" + FilterValue + "\"";
            }
            bool exists = _db.Exists(SQL, key);
            
            if (exists)
            {
                SQL = "UPDATE " + _tableName + " SET Value = ? WHERE Key = ?";
                if (FilterColumn != "")
                {
                    SQL += " AND " + FilterColumn + " = \"" + FilterValue + "\"";
                }

                _db.DB.Execute(SQL, _items[key], key);
            }
        }

        private void AssertKeyExists(string key)
        {
            if (key == null) 
                throw new ArgumentNullException("key");
            if (!_items.ContainsKey(key))
                throw new KeyNotExistsException(key);
        }

        public bool ContainsKey(string key)
        {
            return _items.ContainsKey(key);
        }

        /// <summary>
        /// Liest einen Wert beliebigen Typs aus den Einstellungen
        /// </summary>
        /// <typeparam name="T">Der Datentyp in den konvertiert werden soll (von String). Arbeitet derzeit nur mit Typen die IConvertible implementieren</typeparam>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <returns>Den eingelesenen und konvertierten Wert</returns>
        public T GetValue<T>(string key)
        {
            AssertKeyExists(key);
            return (T)Convert.ChangeType(_items[key], typeof(T), new CultureInfo("en-US"));    
        }

        /// <summary>
        /// Schreibt einen Wert beliebigen Typs in die Einstellungen
        /// </summary>
        /// <typeparam name="T">Der Datentyp in den konvertiert werden soll (von String). Arbeitet derzeit nur mit Typen die IConvertible implementieren</typeparam>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <param name="value">Der zu schreibende Wert</param>
        public void SetValue<T>(string key, T value)
        {
            if (_items.ContainsKey(key))
            {
                _items[key] = (string)Convert.ChangeType(value, typeof(string), new CultureInfo("en-US")); 
            }
            else
            {
                _items.Add(key, (string)Convert.ChangeType(value, typeof(string), new CultureInfo("en-US"))); 
            }
            
        }

        /// <summary>
        /// Liest einen Wert als String aus der Datenbank
        /// </summary>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <returns>Der eingelesene Wert als String</returns>
        public string GetString(object key)
        {
            string s = key.ToString();
            AssertKeyExists(s);
            return _items[s];
        }

        /// <summary>
        /// Schreibt einen String in die Datenbank
        /// </summary>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <param name="value">Der zu schreibende String</param>
        public void SetString(string key, string value)
        {
            if (ContainsKey(key))
            {
                _items[key] = value;
            }
            else
            {
                _items.Add(key, value);
            }

            Save(key);
        }

        /// <summary>
        /// Liest einen Wert als Integer aus der Datenbank
        /// </summary>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <returns>Der eingelesene Wert als Integer</returns>
        public int GetInteger(string key)
        {
            AssertKeyExists(key);
            return Convert.ToInt32(_items[key]);
        }

        /// <summary>
        /// Liest einen Wert als Float aus der Datenbank
        /// </summary>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <returns>Der eingelesene Wert als Integer</returns>
        public float GetFloat(string key)
        {
            AssertKeyExists(key);
            return (float)Convert.ToDouble(_items[key], new CultureInfo("en-US"));
        }

        /// <summary>
        /// Schreibt einen Float in die Datenbank
        /// </summary>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <param name="value">Der zu schreibende Float</param> 
        public void SetFloat(string key, float value)
        {
            _items[key] = Convert.ToString(value, new CultureInfo("en-US"));
            Save(key);
        }

        /// <summary>
        /// Schreibt einen Integer in die Datenbank
        /// </summary>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <param name="value">Der zu schreibende Integer</param>
        public void SetInteger(string key, int value)
        {
            _items[key] = Convert.ToString(value);
            Save(key);
        }

        /// <summary>
        /// Liest einen Wert als Boolean aus der Datenbank
        /// </summary>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <returns>Der eingelesene Wert als Boolean</returns>
        public bool GetBool(string key)
        {
            AssertKeyExists(key);
            return Convert.ToBoolean(_items[key]);
        }

        /// <summary>
        /// Schreibt einen Boolean in die Datenbank
        /// </summary>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <param name="value">Der zu schreibende Boolean</param>
        public void SetBool(string key, bool value)
        {
            _items[key] = Convert.ToString(value);
            Save(key);
        }

        /// <summary>
        /// Versucht einen Integer aus der Datenbank zu lesen. Falls der Key nicht enthalten ist, wird ein
        /// Standardwert zurückgegeben
        /// </summary>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <param name="_default">Der Standardwert, falls der Schlüssel nicht enthalten ist</param>
        /// <returns></returns>
        public int GetInteger(string key, int _default)
        {
            return !_items.ContainsKey(key) ? _default : GetInteger(key);
        }

        /// <summary>
        /// Versucht einen Boolean aus der Datenbank zu lesen. Falls der Key nicht enthalten ist, wird ein
        /// Standardwert zurückgegeben
        /// </summary>
        /// <param name="key">Der Schlüssel der Einstellung</param>
        /// <param name="_default">Der Standardwert, falls der Schlüssel nicht enthalten ist</param>
        /// <returns></returns>
        public bool GetBool(string key, bool _default)
        {
            return !_items.ContainsKey(key) ? _default : GetBool(key);
        }

        public IEnumerator GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        public void Clear()
        {
            _items.Clear();
        }
    }
}
#endif