//----------------------------------------------
//          ~ Tastenhacker Library ~
//            � 2013 Thomas Hummes
//----------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

namespace Tastenhacker.Core
{
	public delegate void NotifyEvent();

	[Flags]
	public enum Axis { X = 1, Y = 2, Z = 4 }

	/// <summary>
	/// Determines a direction (currently only 4 sides of a quad are supported) value or a specific "Don't care". Usually used for trigger detection
	/// </summary>
	[Flags]
	public enum Direction
	{
		None = 0,
		Top = 1,
		Left = 2,
		Right = 4,
		Bottom = 8
	};

    public enum Horizontal2
    {
        Left, Right
    }

    public enum Vertical2
    {
        Up, Down
    }

	public static class DirectionHelper
	{
		public static Direction PositionsToDirection(this GameObject self, GameObject other)
		{
			float triggerLeftBorder = self.transform.position.x -
									  (self.transform.localScale.x / 2.0f);
			float triggerRightBorder = self.transform.position.x +
									   (self.transform.localScale.x / 2.0f);

			float triggerTopBorder = self.transform.position.z +
									 (self.transform.localScale.z / 2.0f);
			float triggerBottomBorder = self.transform.position.z -
										(self.transform.localScale.z / 2.0f);

			Dictionary<Direction, float> relativesHorizontal = new Dictionary<Direction, float>();
			Dictionary<Direction, float> relativesVertical = new Dictionary<Direction, float>();

			relativesHorizontal.Add(Direction.Left, Mathf.Abs(other.transform.position.x - triggerLeftBorder));
			relativesHorizontal.Add(Direction.Right, Mathf.Abs(other.transform.position.x - triggerRightBorder));
			relativesVertical.Add(Direction.Top, Mathf.Abs(other.transform.position.z - triggerTopBorder));
			relativesVertical.Add(Direction.Bottom, Mathf.Abs(other.transform.position.z - triggerBottomBorder));

			return (relativesHorizontal.Aggregate((p1, p2) => (p1.Value < p2.Value) ? p1 : p2).Key |
					(relativesVertical.Aggregate((p1, p2) => (p1.Value < p2.Value) ? p1 : p2).Key));
		}

        public static bool IsOpposite(Direction a, Direction b)
        {
            switch (a)
            {
                case Direction.Top:
                    return b == Direction.Bottom;
                case Direction.Bottom:
                    return b == Direction.Top;
                case Direction.Left:
                    return b == Direction.Right;
                case Direction.Right:
                    return b == Direction.Left;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public static Direction GetOpposite(this Direction d)
        {
            switch (d)
            {
                case Direction.Top:
                    return Direction.Bottom;
                case Direction.Bottom:
                    return Direction.Top;
                case Direction.Left:
                    return Direction.Right;
                case Direction.Right:
                    return Direction.Left;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
	}

	public enum ComparisonType
	{
		Less,
		LessOrEqual,
		Equal,
		Greater,
		GreaterOrEqual
	};

	public class RangedInt
	{
		protected bool Equals(RangedInt other)
		{
			return Min == other.Min && Max == other.Max;
		}

		public override int GetHashCode()
		{
			unchecked
			{
				// ReSharper disable NonReadonlyFieldInGetHashCode
				return Min*397 ^ Max;
				// ReSharper restore NonReadonlyFieldInGetHashCode
			}
		}

		public int Min;
		public int Max;

		public RangedInt(int min, int max)
		{
			Min = min;
			Max = max;
		}

		public RangedInt()
			: this(0, 0)
		{
		}

		public RangedInt(RangedInt copyFrom)
			: this(copyFrom.Min, copyFrom.Max)
		{
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((RangedInt) obj);
		}

		public int GetValue()
		{
			Random r = new Random();
			return r.Next(Min, Max + 1);
		}
	}

	public class LimitedInt
	{
		public override int GetHashCode()
		{
			unchecked
			{
				// ReSharper disable NonReadonlyFieldInGetHashCode
				int hashCode = Min;
				hashCode = (hashCode*397) ^ Max;
				hashCode = (hashCode*397) ^ Value;
				return hashCode;
				// ReSharper enable NonReadonlyFieldInGetHashCode
			}
		}

		protected bool Equals(LimitedInt other)
		{
			return Value == other.Value;
		}

		public int Min;
		public int Max;

		private int _value;
		public int Value
		{
			get { return _value; }
			set { _value = Helper.Math.Clamp(value, Min, Max); }
		}

		public LimitedInt(int min, int max, int value)
		{
			Min = min;
			Max = max;
			Value = value;
		}

		public LimitedInt() : this(0, 0, 0) {}
		public LimitedInt(int value) : this(0, 0, value) {}
		public LimitedInt(LimitedInt copyFrom) : this(copyFrom.Min, copyFrom.Max, copyFrom.Value) {}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((LimitedInt)obj);
		}

		public static implicit operator int(LimitedInt l)
		{
			return l.Value;
		}

	    public void Copy(LimitedInt other)
	    {
	        Min = other.Min;
	        Max = other.Max;
	        Value = other.Value;
	    }
	}
}