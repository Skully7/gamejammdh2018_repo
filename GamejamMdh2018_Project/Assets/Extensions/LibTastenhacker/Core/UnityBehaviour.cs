﻿using UnityEngine;
using System.Collections.Generic;
using Tastenhacker.Core.Manager;
using System.Linq;

public class UnityBehaviour : MonoBehaviour
{
    private readonly List<Component> _cachedComponents = new List<Component>();

    private static readonly List<ManagerModule> CachedManagerModules = new List<ManagerModule>();

    public T Component<T>() where T : Component
    {
        if(_cachedComponents.Any(t => t is T))
        {
            return (T) _cachedComponents.First(s => s is T);
        }
        T c = GetComponent<T>();
        if(c == null)
        {
            Debug.LogError(string.Format("The gameObject {0} has no component of type {1}", name, typeof(T)));
        }
        else
        {
            _cachedComponents.Add(c);
            return c;
        }
        return null;
    }

    public T Get<T>() where T : ManagerModule
    {
        if(CachedManagerModules.Any(t => t is T))
        {
            return (T) CachedManagerModules.First(s => s is T);
        }
        T m = Manager.Instance.Get<T>();
        CachedManagerModules.Add(m);
        return m;
    }
}
