﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class DecomposerElement
{
    public string Identifier;
    public string Name;
    public int Modulus;
    public int Value;

    public DecomposerElement() : this("", 1) { }

    public DecomposerElement(string identifier, int modulus)
    {
        Identifier = identifier;
        Modulus = modulus;
        Value = 0;
    }

    public DecomposerElement Clone()
    {
        return ReflectionHelper.Clone(this);
    }
}

[Serializable]
public class CurrencyDecomposer
{
    public List<DecomposerElement> Decomposers;

    public string Name;

    public CurrencyDecomposer()
    {
        Decomposers = new List<DecomposerElement>();
    }

    public string Decompose(int value)
    {
        for (int index = 0; index < Decomposers.Count - 1; index++)
        {
            DecomposerElement d = Decomposers[index];
            d.Value = value % d.Modulus;
            value /= d.Modulus;
        }
        Decomposers.Last().Value = value;
        return ToString();
    }

    public List<DecomposerElement> DecomposeEx(int value)
    {
        List<DecomposerElement> decompositions = new List<DecomposerElement>();

        for (int index = 0; index < Decomposers.Count - 1; index++)
        {
            DecomposerElement d = Decomposers[index];
            d.Value = value % d.Modulus;
            value /= d.Modulus;
            decompositions.Add(d.Clone());
        }

        DecomposerElement lastDecomposer = Decomposers.Last();
        lastDecomposer.Value = value;
        decompositions.Add(lastDecomposer.Clone());
        return decompositions;
    }

    public override string ToString()
    {
        string result = "";
        foreach(DecomposerElement d in Decomposers.Where(p => p.Value != 0))
        {
            result = (d.Identifier + d.Value + " ") + result;
        }
        return result;
    }

    public string DecomposeLong(int value)
    {
        for (int index = 0; index < Decomposers.Count - 1; index++)
        {
            DecomposerElement d = Decomposers[index];
            d.Value = value % d.Modulus;
            value /= d.Modulus;
        }
        Decomposers.Last().Value = value;

        string result = "";
        foreach (DecomposerElement d in Decomposers.Where(p => p.Value != 0))
        {
            result = (d.Value + " " + d.Name + " ") + result;
        }
        return result;
    }
}