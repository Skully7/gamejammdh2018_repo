﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Tastenhacker.DataStructures
{
    public class RingBuffer<T> : IList<T>
    {
        public RingBuffer(int capacity)
        {
            if(capacity <= 0)
                throw new ArgumentException("Must be greater than zero", "capacity");
            Capacity = capacity;
            _buffer = new T[capacity];
        }

        private readonly T[] _buffer;

        private int _position;

        private long _version;

        public int Capacity { get; private set; }

        public int Count { get; private set; }

        public T this[int index]
        {
            get
            {
                if (index < 0 | index >= Count)
                    throw new IndexOutOfRangeException();
                return _buffer[(_position - Count + index)%Capacity];
            }
            set { Insert(index, value); }
        }

        public void Add(T item)
        {
            if (_position == int.MaxValue)
                _position = _position%Capacity;

            _buffer[_position++%Capacity] = item;
            if (Count < Capacity)
                Count++;
            _version++;
        }

        public void Clear()
        {
            for (int i = 0; i < Count; i++)
            {
                _buffer[i] = default (T);
            }
            _position = 0;
            Count = 0;
            _version++;
        }

        public bool Contains(T item)
        {
            return IndexOf(item) != -1;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            for (int i = 0; i < Count; i++)
            {
                array[i + arrayIndex] = _buffer[(_position - Count + i)%Capacity];
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            long version = _version;
            for (int i = 0; i < Count; i++)
            {
                if(version != _version)
                    throw new InvalidOperationException("Collection was modified");
                yield return this[i];
            }
        }

        public int IndexOf(T item)
        {
            for (int i = 0; i < Count; i++)
            {
                T item2 = _buffer[(_position - Count + i)%Capacity];
                if (ReferenceEquals(item, item2))
                    return i;
                if (!item.Equals(null) && item.Equals(item2))
                    return i;
            }
            return -1;
        }

        public void Insert(int index, T item)
        {
            if(index < 0 || index > Count)
                throw new IndexOutOfRangeException();

            if (index == Count)
            {
                Add(item);
                return;
            }

            int count = System.Math.Min(Count, Capacity - 1) - index;
            int index2 = (_position - Count + index) % Capacity;

            for (int i = index2 + count; i > index2; i--)
            {
                int to = i%Capacity;
                int from = (i - 1)%Capacity;
                _buffer[to] = _buffer[from];
            }

            _buffer[index2] = item;
            if (Count < Capacity)
            {
                Count++;
                _position++;
            }
            _version++;
        }

        public bool Remove(T item)
        {
            int index = IndexOf(item);
            if (index == -1)
                return false;
            RemoveAt(index);
            return true;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= Count)
                throw new IndexOutOfRangeException();

            for (int i = index; i < Count - 1; i++)
            {
                int to = (_position - Count + i) % Capacity;
                int from = (_position - Count + i + 1) % Capacity;
                _buffer[to] = _buffer[from];
            }

            int last = (_position - 1) % Capacity;
            _buffer[last] = default (T);
            _position--;
            Count--;
            _version++;
        }

        bool ICollection<T>.IsReadOnly { get { return false; } }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            return this.Aggregate("", (current, entry) => current + (entry.ToString() + System.Environment.NewLine));
        }

        public void Splay(T value)
        {
            if (Contains(value))
            {
                Remove(value);
            }
            Add(value);
        }
    }
}