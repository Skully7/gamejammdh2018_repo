﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

namespace Tastenhacker.DataStructures
{
    public class PickerEmptyException : Exception { }

    /// <summary>
    /// A List of Elements of T and their probabilities of being picked.
    /// </summary>
    /// <typeparam name="T">Type of stored Elements</typeparam>
    public class WeightedPicker<T>
    {
        static int seed = new Random().Next();

        private bool _hasChanged;
        private float _whole;
        private readonly Dictionary<T, float> _dictionary = new Dictionary<T, float>();

        /// <summary>
        /// Gets the number of Elements in this Picker.
        /// </summary>
        public int Count
        {
            get { return _dictionary.Count; }
        }

        public float this[T key]
        {
            get 
            { 
                return _dictionary[key]; 
            }

            set
            {
                _dictionary[key] = value;
                _hasChanged = true;
            }
        }

        private void UpdateWhole()
        {
            _whole = _dictionary.Values.Sum();
        }

        /// <summary>
        /// Returns a random Element of the picker.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="PickerEmptyException">Thrown when there are no Elements in the Picker</exception>
        public T Pick()
        {
            if (_hasChanged)
                UpdateWhole();

            Random rnd = new Random(seed);
            seed += rnd.Next();

            float randomPercentage = rnd.Next((int)(_whole*100)) / 100f;

            return PickManually(randomPercentage);
        }

        /// <summary>
        /// Returns a specific Element of the Picker,
        /// depending on the passed Value.
        /// </summary>
        /// <param name="value">Value between 0 and sum of all Elements' probabilities</param>
        /// <returns></returns>
        /// <exception cref="PickerEmptyException">Thrown when there are no Elements in the Picker</exception>
        public T PickManually(float value)
        {
            if (_hasChanged)
                UpdateWhole();

            value = Mathf.Min(_whole,(Mathf.Max(0, value)));

            float accumulate = 0;
            KeyValuePair<T, float>? last = null;

            foreach (KeyValuePair<T, float> pair in _dictionary)
            {
                if (accumulate > value)
                    break;
                last = pair;
                accumulate += pair.Value;
            }

            if (!last.HasValue)
                throw new PickerEmptyException();

            return last.Value.Key;
        }

        /// <summary>
        /// Adds Element with specific probability.
        /// </summary>
        /// <param name="key">Element to store</param>
        /// <param name="value">Probability</param>
        public void Add(T key, float value)
        {
            _dictionary.Add(key, value);
            _hasChanged = true;
        }

        /// <summary>
        /// Removes specific Element.
        /// </summary>
        /// <param name="key">Element to remove</param>
        /// <returns></returns>
        public bool Remove(T key)
        {
            _hasChanged = true;
            return _dictionary.Remove(key);
        }

        /// <summary>
        /// Clears the Picker, removing all Elements.
        /// </summary>
        public void Clear()
        {
            _dictionary.Clear();
            _hasChanged = true;
        }
    }
}
