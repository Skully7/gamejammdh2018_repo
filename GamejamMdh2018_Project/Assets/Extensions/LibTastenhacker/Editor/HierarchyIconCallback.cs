//----------------------------------------------
//          ~ Tastenhacker Library ~
//            � 2013 Thomas Hummes
//----------------------------------------------

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

namespace Tastenhacker.Editor
{
    [InitializeOnLoad]
    public class HierarchyIconCallback
    {
        private static readonly EditorApplication.HierarchyWindowItemCallback hiearchyItemCallback;
        private static Dictionary<string, Texture2D> _icons;

        // constructor
        static HierarchyIconCallback()
        {
            _icons = new Dictionary<string, Texture2D>();
            if (_icons.Count == 0)
                LoadIcons();
            hiearchyItemCallback = DrawHierarchyIcon;
            EditorApplication.hierarchyWindowItemOnGUI = (EditorApplication.HierarchyWindowItemCallback) Delegate.Combine(EditorApplication.hierarchyWindowItemOnGUI,hiearchyItemCallback);
        }

        [MenuItem("Edit/Reload Callback Icons")]
        private static void DoSomething()
        {
            _icons = new Dictionary<string, Texture2D>();
            LoadIcons();
        }

        private static void LoadIcons()
        {
            TextAsset manifest = Resources.Load("Callback/CallbackIcons") as TextAsset;
            string[] delimiter = {System.Environment.NewLine};
            if (manifest == null)
                return;

            string[] files = manifest.text.Split(delimiter, StringSplitOptions.None);
            foreach (string filename in files)
            {
                UnityEngine.Object obj = Resources.Load("Callback/Icons/" + filename);
                _icons.Add(filename, obj as Texture2D);
            }
        }

        private static void DrawHierarchyIcon(int instanceId, Rect selectionRect)
        {
            GameObject gameObject = EditorUtility.InstanceIDToObject(instanceId) as GameObject;
            if (gameObject == null)
                return;
            if (!_icons.ContainsKey(gameObject.name))
                return;
            Rect rect = new Rect(selectionRect.x + selectionRect.width - 16f, selectionRect.y, 16f, 16f);
            GUI.DrawTexture(rect, _icons[gameObject.name]);
        }
    }
}