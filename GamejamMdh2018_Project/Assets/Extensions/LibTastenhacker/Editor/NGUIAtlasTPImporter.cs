﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

#if NGUI

using System.IO;
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class NGUIAtlasTPImporter : AssetPostprocessor 
{
    public static Dictionary<UIAtlas, TextAsset> Imports = new Dictionary<UIAtlas, TextAsset>();

    void OnPostprocessTexture(Texture2D texture)
    {
        string AtlasPath = Path.ChangeExtension(assetPath, "prefab");
        string TPFilePath = Path.ChangeExtension(assetPath, "txt");

        TextAsset tpAsset = (TextAsset) AssetDatabase.LoadAssetAtPath(TPFilePath, typeof (TextAsset));
        GameObject AtlasObj = (GameObject) AssetDatabase.LoadAssetAtPath(AtlasPath, typeof (GameObject));

        if (tpAsset != null && AtlasObj != null)
        {
            UIAtlas atlas = AtlasObj.GetComponent<UIAtlas>();

            // Ensure that this atlas has valid import settings
            if (atlas.texture != null)
                NGUIEditorTools.ImportTexture(atlas.texture, false, false, !atlas.premultipliedAlpha);

            if (!Imports.ContainsKey(atlas))
            {
                Imports.Add(atlas, tpAsset);
            }
        }
    }

    private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
        foreach(KeyValuePair<UIAtlas, TextAsset> entry in Imports)
        {
            NGUIEditorTools.RegisterUndo("Import Sprites", entry.Key);
            NGUIJson.LoadSpriteData(entry.Key, entry.Value);
            entry.Key.MarkAsChanged();
        }
        Imports.Clear();
    }
}
#endif