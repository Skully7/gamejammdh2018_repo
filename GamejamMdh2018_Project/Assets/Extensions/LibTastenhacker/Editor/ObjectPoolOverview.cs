﻿using UnityEditorHelper;
using UnityEditor;
using UnityEngine;

public class ObjectPoolOverview : EditorWindow
{
    [MenuItem("Tools/Object Pool Overview")]
    public static void ShowWindow()
    {
        GetWindow(typeof(ObjectPoolOverview));
    }

    public void OnEnable()
    {
        string basePath = EditorHelper.GetAssetPath("ObjectPoolOverview.cs") + "ObjectPoolOverview.png";
        Texture titleIcon = AssetDatabase.LoadAssetAtPath(basePath, typeof(Texture)) as Texture;
        titleContent = new GUIContent(" Object Pools", titleIcon);
    }

    public void OnGUI()
    {
        if (!EditorApplication.isPlaying)
        {
            GUILayout.Space(32);
            EditorGUILayout.HelpBox("Object Pool Statistics are only available in play mode", MessageType.Info);
            return;
        }

        foreach (ObjectPool pool in ObjectPool.Pools)
        {
            EditorGUILayout.BeginVertical("Box");
            {
                EditorGUILayout.ObjectField("GameObject", pool.gameObject, typeof (GameObject), true);
                EditorGUILayout.LabelField(pool.PoolItemName + "(" + pool.gameObject.name + ")");
                if (EditorApplication.isPlaying)
                {
                    int max = (pool.Mode == ObjectPoolMode.FixedSize) ? pool.FixedSize : pool.transform.childCount;
                    float value = (pool.ActivePoolObjects * 1.0f) / max;
                    Rect rect = EditorGUILayout.GetControlRect(true, 20);
                    EditorGUI.ProgressBar(rect, value, string.Format("{0} / {1}", pool.ActivePoolObjects, max));
                }
            }
            EditorGUILayout.EndVertical();
        }
    }

    public void OnInspectorUpdate()
    {
        Repaint();
    }
}