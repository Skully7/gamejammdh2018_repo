﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public static class UnityPropertyHelper
{
    public static int IndexOf(this SerializedProperty prop, string value)
    {
        if (!prop.isArray)
            return -1;
        for (int i = 0; i < prop.arraySize; i++)
        {
            if (prop.GetArrayElementAtIndex(i).stringValue.Equals(value))
                return i;
        }
        return -1;
    }
}