//----------------------------------------------
//          ~ Tastenhacker Library ~
//            � 2013 Thomas Hummes
//----------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Tastenhacker.Math;

namespace Tastenhacker.Helper
{
    /// <summary>
    /// Contains helper function for work with .NET Collections
    /// </summary>
    public static class Collections
    {
        /// <summary>
        /// Randomly shuffles a generic list
        /// </summary>
        /// <typeparam name="T">The type of the lists contents</typeparam>
        /// <param name="list">The list to shuffle</param>
        /// <param name="random">The desired random numer generator</param>
        /// <remarks>No cryptographic secure randomization</remarks>
        public static void Shuffle<T>(this IList<T> list, Random random)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            list.Shuffle(new MersenneTwister((uint) Guid.NewGuid().GetHashCode()));
        }

        public static T PickRandom<T>(this IList<T> source)
        {
            return source[UnityEngine.Random.Range(0, source.Count - 1)];
        }

        public static void TryAdd<TK, TV>(this Dictionary<TK, TV> dictionary, TK key, TV value)
        {
            if (dictionary.ContainsKey(key))
                return;
            dictionary.Add(key, value);
        }

        public static void TryRemove<TK, TV>(this Dictionary<TK, TV> dictionary, TK key)
        {
            if (!dictionary.ContainsKey(key))
                return;
            dictionary.Remove(key);
        }

        public static void TryAdd<T>(this IList<T> collection, T value)
        {
            if (collection.Contains(value))
                return;
            collection.Add(value);
        }

        public static void TryRemove<T>(this IList<T> collection, T value)
        {
            if (collection.Contains(value))
                collection.Remove(value);
        }

        public static void TryRemove<T>(this HashSet<T> collection, T value)
        {
            if (collection.Contains(value))
                collection.Remove(value);
        }

        public static bool IsLast<T>(this List<T> list, T element)
        {
            return (list.IndexOf(element) == list.Count - 1);
        }

        public static T[] Shift<T>(this T[] array, int positions)
        {
            positions %= array.Length;
            T[] copy = new T[array.Length];
            Array.Copy(array, 0, copy, array.Length - positions, positions);
            Array.Copy(array, positions, copy, 0, array.Length - positions);
            return copy;
        }

        public static int FindNextLowerGap(this IEnumerable<int> list, int defaultValue)
        {
            if (!list.Any())
            {
                return defaultValue;
            }

            IOrderedEnumerable<int> liste = list.OrderBy(i => i);
            int min = liste.Min();
            int max = liste.Max();
            IEnumerable<int> gapList = Enumerable.Range(min, max - min + 1).Except(liste).ToArray();
            return !gapList.Any() ? defaultValue : gapList.Last();
        }
    }
}