﻿using UnityEngine;

/**
 * CustomGizmo.cs
 * Author: Thomas Hummes
 * Summary: Hilfsklasse, die dem übergeordneten Objekt einen benutzerdefinierten Gizmo gibt.
 * 	        Das Bild muss im Unterorder "Gizmos" liegen und sollte 32x32 px groß sein.
 * 		
 * **/
public class CustomGizmo : MonoBehaviour {
	

	/* Der Dateiname des Gizmo-Icons. Die Endung ist optional, Unity akzeptiert es mit und ohne */
	public string IconName;
	
	void OnDrawGizmos()
	{
		Gizmos.DrawIcon(this.gameObject.transform.position, IconName);	
	}
		
}
