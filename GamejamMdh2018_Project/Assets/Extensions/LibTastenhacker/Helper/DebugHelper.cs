﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

using System.Diagnostics;
using System.Reflection;
using UnityEngine;

namespace Tastenhacker.Helper
{
    public static class DebugHelper
    {
        public static MethodBase GetCaller(int depth = 2)
        {
        #if UNITY_WINRT
            return null;
        #else
            StackTrace stackTrace = new StackTrace();
            return stackTrace.GetFrame(1).GetMethod();
        #endif
        }

        public static string GetPath(this Transform current)
        {
            if (current.parent == null)
                return "/" + current.name;
            return current.parent.GetPath() + "/" + current.name;
        }
    }
}
