//----------------------------------------------
//          ~ Tastenhacker Library ~
//            � 2013 Thomas Hummes
//----------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;

namespace Tastenhacker.Helper
{
    public static class EnumUtil
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof (T)).Cast<T>();
        }

        public static int ValueCount<T>()
        {
            return Enum.GetValues(typeof (T)).Length;
        }

        static readonly Random Random = new Random();

        public static T RandomEnumValue<T>(Func<T, bool> where)
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Where(where).OrderBy(x => Random.Next()).FirstOrDefault();
        }

        public static T RandomEnumValue<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().OrderBy(x => Random.Next()).FirstOrDefault();
        }

        /// <summary>
        /// Pr�ft ob ein Enum ein bestimmtes Flag gesetzt hat
        /// </summary>
        /// <typeparam name="T">Datentyp des Enums</typeparam>
        /// <param name="type">Der Enum selbst</param>
        /// <param name="value">Flag auf das gepr�ft werden soll</param>
        /// <returns></returns>
        public static bool Has<T>(this Enum type, T value)
        {
            try
            {
                if ((int)(object) type == 0)
                    return false;
                return (((int)(object)type & (int)(object)value) == (int)(object)value);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// F�gt ein Flag einem Enum hinzu
        /// </summary>
        public static T Add<T>(this Enum type, T value)
        {
            try
            {
               return (T)(object)(((int)(object)type | (int)(object)value));
            }
            catch (Exception ex)
            {
                throw new ArgumentException(string.Format("Could not append value from enumerated type '{0}'.", typeof(T).Name), ex);
            }
        }

        /// <summary>
        /// Entfernt ein Flag aus einem Enum
        /// </summary>
        public static T Remove<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)(((int)(object)type & ~(int)(object)value));
            }
            catch (Exception ex)
            {
                throw new ArgumentException(string.Format("Could not remove value from enumerated type '{0}'.",typeof(T).Name), ex);
            }
        }

        public static T Next<T>(this T src) where T : struct
        {
            T[] arr = (T[]) Enum.GetValues(src.GetType());
            int j = Array.IndexOf(arr, src) + 1;
            return (arr.Length == j) ? arr[0] : arr[j];
        }

        public static T Prev<T>(this T src) where T : struct
        {
            T[] arr = (T[])Enum.GetValues(src.GetType());
            int j = Array.IndexOf(arr, src) - 1;
            return (j < 0) ? arr[arr.Length - 1] : arr[j];
        }
    }
}

