﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

using UnityEngine;

namespace Tastenhacker.Helper
{
    public static class ExtendedGizmos
    {
        public static void GizmoArrow(Vector3 pos, Vector3 direction, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
        {
            GizmoArrow(pos, direction, Gizmos.color, arrowHeadLength, arrowHeadAngle);
        }
     
        public static void GizmoArrow(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
        {
            Gizmos.color = color;
            Gizmos.DrawRay(pos, direction);

            Vector3 right;
            Vector3 left;
            ConstructVectors(direction, arrowHeadAngle, out right, out left);
            Gizmos.DrawRay(pos + direction, right * arrowHeadLength);
            Gizmos.DrawRay(pos + direction, left * arrowHeadLength);
        }

        private static void ConstructVectors(Vector3 direction, float arrowHeadAngle, out Vector3 right, out Vector3 left)
        {
            right = Quaternion.LookRotation(direction)*Quaternion.Euler(0, 180 + arrowHeadAngle, 0)*new Vector3(0, 0, 1);
            left = Quaternion.LookRotation(direction)*Quaternion.Euler(0, 180 - arrowHeadAngle, 0)*new Vector3(0, 0, 1);
        }

        public static void ForDebug(Vector3 pos, Vector3 direction, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
        {
            Debug.DrawRay(pos, direction);
           
            Vector3 right;
            Vector3 left;
            ConstructVectors(direction, arrowHeadAngle, out right, out left);
            Debug.DrawRay(pos + direction, right * arrowHeadLength);
            Debug.DrawRay(pos + direction, left * arrowHeadLength);
        }
        public static void ForDebug(Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f)
        {
            Debug.DrawRay(pos, direction, color);
           
            Vector3 right;
            Vector3 left;
            ConstructVectors(direction, arrowHeadAngle, out right, out left);
            Debug.DrawRay(pos + direction, right * arrowHeadLength, color);
            Debug.DrawRay(pos + direction, left * arrowHeadLength, color);
        }
    }
}