using System.Globalization;
using System;
using UnityEngine;
using Tastenhacker.Core.Manager;
using UnityEngine.Events;
using UnityEditorHelper;

/// <summary>
/// Simuliert eine Spielzeit mit passenden Delegaten f�r gew�nschte Events. Die maximale Aufl�sung betr�gt 1 (Spiel-)Minute
/// </summary>
public class GameTimeSimulator : ManagerModule
{
    private DateTime _dateTime;

    public Vector2 NightHours;

    [Limit(0)]
    public float DayLengthInMinutes;

    public DateTime DateTime
    {
        get { return _dateTime; }
        set
        {
            _dateTime = value;
            if (OnChange != null)
                OnChange.Invoke();
        }
    }

    public bool IsNighttime
    {
        get
        {
            return (Hour >= NightHours.x) || (Hour <= NightHours.y);
        }
    }

    public float PercentageOfDay
    {
        get { return (float) (DateTime.TimeOfDay.TotalDays - Math.Truncate(DateTime.TimeOfDay.TotalDays)); }
    }

    public UnityEvent OnChange;
    public UnityEvent OnNextDay;
    public UnityEvent OnNextMonth;
    public UnityEvent OnNextYear;
    public UnityEvent OnNextHour;
    public UnityEvent OnMove;

    public int Year { get; private set; }
    public int Month { get; private set; }
    public int Day { get; private set; }
    public int Hour { get; private set; }
    public int Minute { get; private set; }
    public int Second { get; private set; }

    public override void OnAwake()
    {
        base.OnAwake();
        DateTime = DateTime.Now;
        Set(DateTime.Year, DateTime.Day, DateTime.Month, DateTime.Hour, DateTime.Minute);
    }

    private void UpdateCounter()
    {
        int newYear = DateTime.Year;
        if (newYear != Year)
        {
            OnNextYear.Invoke();
            Year = newYear;
        }

        int newMonth = DateTime.Month;
        if (newMonth != Month)
        {
            OnNextMonth.Invoke();
            Month = newMonth;
        }

        int newDay = DateTime.Day;
        if (newDay != Day)
        {
            OnNextDay.Invoke();
            Day = newDay;
        }

        int newHour = DateTime.Hour;
        if (newHour != Hour)
        {
            OnNextHour.Invoke();
            Hour = newHour;
        }
        Minute = DateTime.Minute;
        Second = DateTime.Second;
        OnMove.Invoke();
    }

    public void Set(int year, int day, int month, int hour, int minute)
    {
        DateTime = new DateTime(year, month, day, hour, minute, 0);
        Year = year;
        Month = month;
        Day = day;
        Hour = hour;
   }

    public void MoveByMinutes(int minutes)
    {
        DateTime = DateTime.AddMinutes(minutes);
        UpdateCounter();
    }

    public void MoveBySeconds(float seconds)
    {
        DateTime = DateTime.AddSeconds(seconds);
        UpdateCounter();
    }

    public void MoveByHours(int hours)
    {
        DateTime = DateTime.AddHours(hours);
        UpdateCounter();
    }

    public void MoveByDays(int days)
    {
        DateTime = DateTime.AddDays(days);
        UpdateCounter();
    }

    public void SetYear(int year)
    {
        if(year <= 1980) 
            throw new ArgumentOutOfRangeException("year", year.ToString(CultureInfo.InvariantCulture));

        DateTime = new DateTime(year, DateTime.Month, Math.Min(DateTime.Day, DateTime.DaysInMonth(year, DateTime.Month)), DateTime.Hour, DateTime.Minute, DateTime.Second);
    }

    public void SetMonth(int month)
    {
        if((month <= 0) || (month > 12))
            throw new ArgumentOutOfRangeException("month", month.ToString(CultureInfo.InvariantCulture));

        DateTime = new DateTime(DateTime.Year, month, Math.Min(DateTime.Day, DateTime.DaysInMonth(DateTime.Year, month)), DateTime.Hour, DateTime.Minute, DateTime.Second);
    }

    public void SetDay(int day)
    {
        if((day <= 0) || (day > DateTime.DaysInMonth(DateTime.Year, DateTime.Month)))
            throw new ArgumentOutOfRangeException("day", day.ToString(CultureInfo.InvariantCulture));

        DateTime = new DateTime(DateTime.Year, DateTime.Month, day, DateTime.Hour, DateTime.Minute, DateTime.Second);
    }

    public void SetHour(int hour)
    {
        if((hour < 0) || (hour >= 24))
            throw new ArgumentOutOfRangeException("hour", hour.ToString(CultureInfo.InvariantCulture));

        DateTime = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, hour, DateTime.Minute, DateTime.Second);
    }

    public void SetMinute(int minute)
    {
        if((minute < 0) || (minute >= 60))
            throw new ArgumentOutOfRangeException("minute", minute.ToString(CultureInfo.InvariantCulture));

        DateTime = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, DateTime.Hour, minute, DateTime.Second);
    }

    public void Update()
    {
        const int MinutesPerDay = 1440;
        float scaling = MinutesPerDay / DayLengthInMinutes;
        MoveBySeconds(Time.deltaTime * scaling);
        OnMove.Invoke();
    }
}
