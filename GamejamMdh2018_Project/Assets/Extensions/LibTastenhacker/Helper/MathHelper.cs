//----------------------------------------------
//          ~ Tastenhacker Library ~
//            � 2013 Thomas Hummes
//----------------------------------------------

using System.Collections.Generic;
using Tastenhacker.Core;
using UnityEngine;

namespace Tastenhacker.Helper
{
    /// <summary>
    /// Mathematical helper routines
    /// </summary>
    public static class Math
    {

        public const double Epsilon = 0.001;
        public const float Epsilonf = 0.001f;

        /// <summary>
        /// Rounds a value to the nearest multiple of a number (bankers rounding)
        /// </summary>
        /// <param name="value">the value to be rounded</param>
        /// <param name="multiple">the nearest multiple the value should be rounded to</param>
        /// <returns></returns>
        public static float RoundToMultiple(float value, float multiple)
        {
            return Mathf.Round(value/multiple)*multiple;
        }

        public static float FloorToMultiple(float value, float multiple)
        {
            float newValue = RoundToMultiple(value, multiple);
            return (newValue > value) ? newValue - multiple : newValue;
        }

        public static Vector3 FloorToMultiple(Vector3 value, float multiple)
        {
            return new Vector3(FloorToMultiple(value.x, multiple), FloorToMultiple(value.y, multiple), FloorToMultiple(value.z, multiple));
        }

        /// <summary>
        /// Checks if a value is within a given limit
        /// </summary>
        /// <param name="value">the value to be checked</param>
        /// <param name="lowerLimit">the lower limit (inclusive)</param>
        /// <param name="upperLimit">the upper limit (inclusive)</param>
        /// <returns></returns>
        public static bool Between(float value, float lowerLimit, float upperLimit)
        {
            return ((value >= lowerLimit) && (value <= upperLimit));
        }

        /// <summary>
        /// Checks if a value is within a given limit
        /// </summary>
        /// <param name="value">the value to be checked</param>
        /// <param name="lowerLimit">the lower limit (inclusive)</param>
        /// <param name="upperLimit">the upper limit (inclusive)</param>
        /// <returns></returns>
        public static bool Between(int value, int lowerLimit, int upperLimit)
        {
            return ((value >= lowerLimit) && (value <= upperLimit));
        }

        public static void ChangeLayerRecursive(Transform parent, int layer)
        {
            foreach (Transform child in parent)
            {
                child.gameObject.layer = layer;
                ChangeLayerRecursive(child, layer);
            }
        }

        public static int Clamp(int value, int lowerLimit, int upperLimit)
        {
            if (value >= upperLimit)
                value = upperLimit;
            if (value <= lowerLimit)
                value = lowerLimit;
            return value;
        }

        /// <summary>
        /// Rundet einen Vektor auf ganzzahlige Komponenten ab
        /// </summary>
        /// <param name="vec"></param>
        /// <param name="axis"></param>
        /// <returns></returns>
        public static Vector3 TruncVector(Vector3 vec, Axis axis)
        {
            Vector3 res = vec;
            if (axis.Has(Axis.X))
                res.x = Mathf.FloorToInt(res.x);
            if (axis.Has(Axis.Y))
                res.y = Mathf.FloorToInt(res.y);
            if (axis.Has(Axis.Z))
                res.z = Mathf.FloorToInt(res.z);
            return res;
        }

        public static Vector3 RoundVectorToMultiples(Vector3 vec, int multiple)
        {
            return new Vector3
            {
                x = RoundToMultiple(vec.x, multiple),
                y = RoundToMultiple(vec.y, multiple),
                z = RoundToMultiple(vec.z, multiple)
            };
        }

        public static float RoundToSignificantDigits(this float d, int digits)
        {
            if (d == 0)
                return 0;

            float scale = Mathf.Pow(10, Mathf.Floor(Mathf.Log10(Mathf.Abs(d))) + 1);
            return (float) (scale * System.Math.Round(d / scale, digits));
        }

        public static Vector3 RoundVector(Vector3 vec)
        {
            return new Vector3
            {
                x = Mathf.RoundToInt(vec.x),
                y = Mathf.RoundToInt(vec.y),
                z = Mathf.RoundToInt(vec.z)
            };
        }

        public static string ColorToHex(this Color color, bool includeBrackets)
        {
            Color32 color32 = color;
            string hex = color32.r.ToString("X2") + color32.g.ToString("X2") + color32.b.ToString("X2");
            return  (includeBrackets) ? "[" + hex + "]" : hex;
        }
    }

    public class Tuple<T1, T2>
    {
        protected bool Equals(Tuple<T1, T2> other)
        {
            return EqualityComparer<T1>.Default.Equals(Item1, other.Item1) && EqualityComparer<T2>.Default.Equals(Item2, other.Item2);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (EqualityComparer<T1>.Default.GetHashCode(Item1)*397) ^ EqualityComparer<T2>.Default.GetHashCode(Item2);
            }
        }

        public readonly T1 Item1;
        public readonly T2 Item2;

        public Tuple(T1 item1, T2 item2)
        {
            Item1 = item1;
            Item2 = item2;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Tuple<T1, T2>) obj);
        }
    }

    [System.Serializable]
    public class Vector2i
    {
        protected bool Equals(Vector2i other)
        {
            return X == other.X && Y == other.Y;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X*397) ^ Y;
            }
        }

        public int X;
        public int Y;

        public Vector2i() : this(0,0) { }

        public Vector2i(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static implicit operator Vector2i(Vector2 v)
        {
            Vector2i vi = new Vector2i {X = Mathf.FloorToInt(v.x), Y = Mathf.FloorToInt(v.y)};
            return vi;
        }

        public static implicit operator Vector2(Vector2i v)
        {
            return new Vector2(v.X, v.Y);
        }

        public static Vector2i operator -(Vector2i a, Vector2i b)
        {
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }

        public static Vector2i operator +(Vector2i a, Vector2i b)
        {
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }

        public override string ToString()
        {
            return "(" + X + "," + Y + ")";
        }

        public override bool Equals(object obj)
        {
            if (obj is Vector2i)
                return (((obj as Vector2i).X == X) && ((obj as Vector2i).Y == Y));
            return base.Equals(obj);
        }
    }

    [System.Serializable]
    public class Margin4i
    {
        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = Top;
                hashCode = (hashCode*397) ^ Left;
                hashCode = (hashCode*397) ^ Right;
                hashCode = (hashCode*397) ^ Bottom;
                return hashCode;
            }
        }

        public int Top;
        public int Left;
        public int Right;
        public int Bottom;

        public Margin4i()
        {
            Top = 0;
            Left = 0;
            Right = 0;
            Bottom = 0;
        }

        public Margin4i(int top, int left, int right, int bottom)
        {
            Top = top;
            Left = left;
            Right = right;
            Bottom = bottom;
        }

        public override string ToString()
        {
            return string.Format("(Top={0}, Left={1}, Right={2}, Bottom={3}", Top, Left, Right, Bottom);
        }

        public override bool Equals(object obj)
        {
            if (obj is Margin4i)
                return (((obj as Margin4i).Top == Top) && ((obj as Margin4i).Left == Left) &&
                        ((obj as Margin4i).Right == Right) && ((obj as Margin4i).Bottom == Bottom));
            return base.Equals(obj);
        }
    }
}