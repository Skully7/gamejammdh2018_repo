﻿using System;
using System.Linq;
using System.Reflection;

public static class ReflectionHelper
{
    public static Type[] GetEnumerableOfType<T>()
    {
        return (from domainAssembly in AppDomain.CurrentDomain.GetAssemblies()
            from assemblyType in domainAssembly.GetTypes()
            where typeof (T).IsAssignableFrom(assemblyType)
            select assemblyType).ToArray();
    }

    public static T Clone<T>(T source) where T : new()
    {
        T obj = new T();
        foreach (FieldInfo field in source.GetType().GetFields())
        {
            field.SetValue(obj, field.GetValue(field));
        }
        return obj;
    }

    public static void ShallowCopy<T>(this T self, T copyFrom)
    {
        foreach (FieldInfo field in copyFrom.GetType().GetFields())
        {
            field.SetValue(self, field.GetValue(field));
        }
    }

    public static bool HasAttribute<T>(this Type type, out T attribute) where T : Attribute
    {
        if (!type.IsDefined(typeof (T), true))
        {
            attribute = null;
            return false;
        }
        attribute = (T) type.GetCustomAttributes(typeof (T), true).First();
        return true;
    }

    public static Type[] GetTypesWithAttribute<T>() where T : Attribute
    {
        return (from domainAssembly in AppDomain.CurrentDomain.GetAssemblies()
                from assemblyType in domainAssembly.GetTypes()
                where Attribute.IsDefined(assemblyType, typeof(T))
                select assemblyType).ToArray();
    }
}