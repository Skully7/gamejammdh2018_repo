﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

using UnityEngine;

namespace Tastenhacker.Helper
{
    public static class ResourcesHelper
    {
        public static GameObject LoadAndInstantiateModel(string path, Transform parent = null)
        {
            GameObject go = Resources.Load<GameObject>(path);
            go = (GameObject) Object.Instantiate(go);
            if (parent != null)
            {
                go.transform.parent = parent;
                go.transform.localPosition = Vector3.zero;
                go.transform.localRotation = Quaternion.identity;
                go.transform.localScale = Vector3.one;
            }
            return go;
        }
    }
}
