//----------------------------------------------
//          ~ Tastenhacker Library ~
//            � 2013 Thomas Hummes
//----------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using Tastenhacker.Core;

namespace Tastenhacker.Helper
{
    /// <summary>
    /// Contains helper methods for working with strings
    /// </summary>
    public static class Strings
    {
        /// <summary>
        /// Replaces the first occurrence of a substring in a string
        /// </summary>
        /// <param name="text">The source text in which the search/replace action should be performed</param>
        /// <param name="search">substring that should be searched</param>
        /// <param name="replace">string that should be used to replace the first occurence of "search"</param>
        /// <returns>the string "text" with the first occurence of "search" replaced by "replace"</returns>
        public static string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search, System.StringComparison.Ordinal);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }

        /// <summary>
        /// Replaces the first occurrence of a substring within a string
        /// </summary>
        /// <param name="text">The source text in which the search/replace action should be performed</param>
        /// <param name="search">substring that should be searched</param>
        /// <param name="replace">string that should be used to replace the first occurence of "search"</param>
        public static void ReplaceFirstInline(ref string text, string search, string replace)
        {
            int pos = text.IndexOf(search, System.StringComparison.Ordinal);
            if (pos > 0)
            {
                text = text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
            }
        }

        public static string FormatEx(this string s, Dictionary<string, IGettable> resources)
        {
            Dictionary<string, string> masks = new Dictionary<string, string> {{s, s}};
            return s.FormatEx(masks, resources);
        }

        public static string FormatEx(this string s, Dictionary<string, string> masks, Dictionary<string, IGettable> resources)
        {
            foreach (KeyValuePair<string, string> mask in masks)
            {
                if (mask.Value.Contains(":"))
                {
                    string[] elements = mask.Value.Split(':');
                    if (elements.Length > 2)
                        throw new FormatException("Splittin resulted in more than one group");
                    if (!resources.ContainsKey(elements[0]))
                    {
                        s = s.Replace(mask.Key, mask.Value);
                        continue;
                    }

                    s = s.Replace(mask.Key, resources[elements[0]].GetString(elements[1]));
                }
                else
                {
                    s = s.Replace(mask.Key, mask.Value);
                }
            }

            return s;
        }


        public static string FormatEx(this string s, string mask, Dictionary<string, IGettable> resources)
        {

                if (mask.Contains(":"))
                {
                    string[] elements = mask.Split(':');
                    if (elements.Length > 2)
                        throw new FormatException("Splittin resulted in more than one group");

                    s = s.Replace(mask, resources[elements[0]].GetString(elements[1]));
                }
                else
                {
                    s = s.Replace(mask, mask);
                }
            

            return s;
        }

        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            System.Random random = new System.Random();
            for (int i = 0; i < size; i++)
            {
                char ch = Convert.ToChar(Convert.ToInt32(System.Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
    }
}