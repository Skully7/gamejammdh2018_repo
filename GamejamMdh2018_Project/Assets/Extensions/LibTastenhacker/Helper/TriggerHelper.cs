//----------------------------------------------
//          ~ Tastenhacker Library ~
//            � 2013 Thomas Hummes
//----------------------------------------------

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Tastenhacker.Core;

namespace Tastenhacker.Helper
{
    /// <summary>
    /// Contains helper methods for working with trigger
    /// </summary>
    public static class Trigger
    {
        /// <summary>
        /// Calculates the closest side from which a gameObject has entered another gameObject. Useful for trigger restrictions
        /// </summary>
        /// <remarks>Currently refers to global space only and only works correctly with box shapes</remarks>
        /// <param name="self">The gameObject that is entered</param>
        /// <param name="other">The gameObject that is entering</param>
        /// <returns>The closest direction vertical and the closed horizontal entering direction (as flags)</returns>
        /// <todo>Include third axis (up, down)</todo>
        public static Direction PositionsToDirection(GameObject self, GameObject other)
        {
            float TriggerLeftBorder = self.transform.position.x - (self.transform.localScale.x/2.0f);
            float TriggerRightBorder = self.transform.position.x + (self.transform.localScale.x/2.0f);
            float TriggerTopBorder = self.transform.position.z + (self.transform.localScale.z/2.0f);
            float TriggerBottomBorder = self.transform.position.z - (self.transform.localScale.z/2.0f);

            // Distinguish between vertical and horizontal entering
            Dictionary<Direction, float> relativesHorizontal = new Dictionary<Direction, float>();
            Dictionary<Direction, float> relativesVertical = new Dictionary<Direction, float>();

            relativesHorizontal.Add(Direction.Left, Mathf.Abs(other.transform.position.x - TriggerLeftBorder));
            relativesHorizontal.Add(Direction.Right, Mathf.Abs(other.transform.position.x - TriggerRightBorder));
            relativesVertical.Add(Direction.Top, Mathf.Abs(other.transform.position.z - TriggerTopBorder));
            relativesVertical.Add(Direction.Bottom, Mathf.Abs(other.transform.position.z - TriggerBottomBorder));

            // extract key with the closed value (= distance)
            return (relativesHorizontal.Aggregate((p1, p2) => (p1.Value < p2.Value) ? p1 : p2).Key |
                    (relativesVertical.Aggregate((p1, p2) => (p1.Value < p2.Value) ? p1 : p2).Key));
        }
    }
}