//----------------------------------------------
//          ~ Tastenhacker Library ~
//            � 2013 Thomas Hummes
//----------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Tastenhacker.Helper
{
    public static class UnityHelper
    {
        private static void _getAllChildren(ref List<GameObject> list, GameObject parent)
        {
            list.Add(parent);
            for (int i = 0; i < parent.transform.childCount; i++)
            {
                _getAllChildren(ref list, parent.transform.GetChild(i).gameObject);
            }
        }

        public enum ChildListMode { SingleLayer, Recursive }

        public static List<GameObject> GetAllChildren(this GameObject root, ChildListMode listmode = ChildListMode.Recursive)
        {
            List<GameObject> list = new List<GameObject>();
            _getAllChildren(ref list, root);
            list.RemoveAll(p => p == root);
            if (listmode == ChildListMode.SingleLayer)
            {
                list.RemoveAll(p => p.transform.parent != root.transform);
            }
            return list;
        }

        public enum DestroyMode { Normal, Immediate }

        public static void DestroyAllChildren(this GameObject root, DestroyMode mode)
        {
            List<GameObject> list = GetAllChildren(root);
            foreach (GameObject child in list)
            {
                if (child != null)
                {
                    switch (mode)
                    {
                        case DestroyMode.Normal:
                            GameObject.Destroy(child);
                            break;

                        case DestroyMode.Immediate:
                            GameObject.DestroyImmediate(child);
                            break;

                        default:
                            throw new ArgumentOutOfRangeException("mode");
                    }
                }
            }
        }

        public static List<T> GetComponentsInAllChildren<T>(GameObject root) where T : Component
        {
            List<GameObject> list = GetAllChildren(root);
            return list.Select(obj => obj.GetComponent<T>()).Where(comp => comp != null).ToList();
        }

        public static T GetComponentInParents<T>(this GameObject self) where T : Component
        {
            Transform current = self.transform;
            while (current.parent != null)
            {
                current = current.parent;
                T comp = current.GetComponent<T>();
                if (comp != null)
                    return comp;
            }
            return null;
        }

        public static Transform GetChildByName(this GameObject self, string name)
        {
            Transform current = self.transform;
            for (int i = 0; i < current.transform.childCount; i++)
            {
                Transform curChild = current.transform.GetChild(i);
                if (curChild.name.Equals(name))
                    return curChild;
            }
            return null;
        }

        public static Rect BoundsToScreenSpace(this Bounds bounds, Camera camera)
        {
            Vector3 origin = camera.WorldToScreenPoint(new Vector3(bounds.min.x, bounds.min.y, bounds.center.z));
            Vector3 extents = camera.WorldToScreenPoint(new Vector3(bounds.max.x, bounds.min.y, bounds.center.z));
            return new Rect(origin.x, Screen.height - origin.y, extents.x - origin.x, origin.y - extents.y);
        }

        public static GameObject InstantiateEx(this GameObject prefab, Transform parent, Vector3 localPosition, Vector3 localScale)
        {
            GameObject go = GameObject.Instantiate(prefab) as GameObject;
            go.transform.parent = parent;
            go.transform.localPosition = localPosition;
            go.transform.localScale = localScale;
            return go;
        }

        public static string ToHexStringRGB(this Color32 color)
        {
            return color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        }

        public static Color HexToColor(this string hex)
        {
            if (hex.Length == 0)
                return new Color32(0, 0, 0, 0);
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            return new Color32(r, g, b, 255);
        }
    }
}