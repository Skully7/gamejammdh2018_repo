﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

using Tastenhacker.Core;
using UnityEngine;

namespace Tastenhacker.Helper
{
    public static class VectorHelper
    {
        private static readonly Vector2[] BorderVectors = new Vector2[4];

        static VectorHelper()
        {
            BorderVectors[0] = new Vector2(-1, 0);
            BorderVectors[1] = new Vector2(1, 0);
            BorderVectors[2] = new Vector2(0, 1);
            BorderVectors[3] = new Vector2(0, -1);            
        }

        public static Vector3 ProjectToViewportEdge(this Vector3 pos, out Direction affectedEdges, out Direction affectedMainEdge)
        {
            affectedEdges = Direction.None;
            affectedMainEdge = Direction.None;
            
            if (IsBehindCamera(pos))
            {
                pos = FlipViewport(pos);
            }

            Vector2 viewportCenter = new Vector2(0.5f, 0.5f);
            Vector2 direction = viewportCenter - (Vector2) pos;

            float m = (viewportCenter.y - pos.y) / (viewportCenter.x - pos.x);
            float b = -m * pos.x + pos.y;
            switch (GetClosestViewportBorderIndex(direction))
            {
                case 0:
                    pos.x = 1;
                    pos.y = Mathf.Clamp01(Line(pos.x, m, b));
                    affectedMainEdge = Direction.Right;
                    affectedEdges = affectedMainEdge | ((pos.y > 0.5f) ? Direction.Top : Direction.Bottom);
                    break;
                case 1:
                    pos.x = 0;
                    pos.y = Mathf.Clamp01(Line(pos.x, m, b));
                    affectedMainEdge = Direction.Left;
                    affectedEdges = affectedMainEdge | ((pos.y > 0.5f) ? Direction.Top : Direction.Bottom);
                    break;
                case 2:
                    pos.x = Mathf.Clamp01(InverseLine(pos.y, b, m));
                    pos.y = 0;
                    affectedMainEdge = Direction.Bottom;
                    affectedEdges = affectedMainEdge | ((pos.x > 0.5f) ? Direction.Right : Direction.Left);
                    break;
                case 3:
                    pos.x = Mathf.Clamp01(InverseLine(pos.y, b, m));
                    pos.y = 1;
                    affectedMainEdge = Direction.Top;
                    affectedEdges = affectedMainEdge | ((pos.x > 0.5f) ? Direction.Right : Direction.Left);
                    break;
                default:
                    Debug.LogWarning("Error finding closest viewport border");
                    break;
            }
            return pos;
        }

        private static float InverseLine(float y, float b, float m)
        {
            return (y - b) / m;
        }

        private static float Line(float x, float m, float b)
        {
            return m * x + b;
        }

        public static int GetClosestViewportBorderIndex(Vector2 direction)
        {
            int maxIndex = -1;
            float maxValue = 0;
            for (int index = 0; index < BorderVectors.Length; index++)
            {
                float d = Vector3.Dot(BorderVectors[index], direction);
                if (d > maxValue)
                {
                    maxValue = d;
                    maxIndex = index;
                }
            }
            return maxIndex;
        }

        private static Vector3 FlipViewport(Vector3 pos)
        {
            pos.y *= -1;
            pos.x *= -1;
            return pos;
        }

        private static bool IsBehindCamera(Vector3 pos)
        {
            return pos.z < 0;
        }
    }
}