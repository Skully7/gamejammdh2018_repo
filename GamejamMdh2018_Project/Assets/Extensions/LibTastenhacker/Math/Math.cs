﻿
//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

using UnityEngine;

namespace Tastenhacker.Math
{
    public static class Math
    {
        public static Vector2 BoundsToScreenRect(this Bounds bounds)
        {
            // Get mesh origin and farthest extent (this works best with simple convex meshes)
            Vector3 origin = Camera.main.WorldToScreenPoint(new Vector3(bounds.min.x, bounds.max.y, 0f));
            Vector3 extent = Camera.main.WorldToScreenPoint(new Vector3(bounds.max.x, bounds.min.y, 0f));

            return new Vector2(Mathf.Abs(extent.x - origin.x), Mathf.Abs(extent.y - origin.y));
        }
    }
}