﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;
using UnityEditorHelper;
#if UNITY_EDITOR
using UnityEditor;
#endif

public enum ObjectPoolMode { FixedSize, Growing }

[Serializable]
public class ObjectPoolEvent : UnityEvent<GameObject>
{

}

public class ObjectPool : MonoBehaviour
{
    private List<GameObject> _pooledObjects;

    public static List<ObjectPool> Pools;

    public ObjectPoolMode Mode;
    [Limit(1)]
    public int FixedSize = 10;
    [Limit(1)]
    public int InitialSize = 10;
    [Limit(1)]
    public int GrowAmount = 1;

    public bool UseFixedLayer;
    [Layer]
    public int FixedLayer;

    public bool UseFixedParent;
    public Transform FixedParent;
    public GameObject Prefab;
    public string PoolItemName;

    public List<GameObject> PrefilledObjects;

    public ObjectPoolEvent OnSpawn;

    public ObjectPoolEvent OnRecycle;

    public bool RemoteControlled;

    static ObjectPool()
    {
        Pools = new List<ObjectPool>();
    }

    public ObjectPool()
    {
        _pooledObjects = new List<GameObject>();
    }

    public static ObjectPool Find(string name)
    {
        return Pools.First(e => e.name.Equals(name));
    }

    public static ObjectPool Find(GameObject go)
    {
        return Pools.First(e => e._pooledObjects.Contains(go));
    }

    public bool IsFull
    {
        get
        {
            return _pooledObjects.All(g => g.activeInHierarchy);
        }
    }

    public int ActivePoolObjects
    {
        get
        {
            return _pooledObjects.Count(g => g.activeInHierarchy);
        }
    }

    public void Clear(bool deletePooledObjects)
    {
        if (deletePooledObjects)
        {
            foreach (GameObject obj in _pooledObjects)
            {
                Destroy(obj);
            }
        }
        _pooledObjects.Clear();
    }

    void Awake()
    {
        _pooledObjects = new List<GameObject>();
        Pools.Add(this);
        _pooledObjects.AddRange(PrefilledObjects);
        if (Prefab == null)
        {
            Debug.LogError("Object pool " + name + " has no prefab assigned");
            return;
        }
        for (int i = 0; i < (Mode == ObjectPoolMode.FixedSize ? FixedSize : InitialSize) - PrefilledObjects.Count; i++)
        {
            SpawnNewPoolItem();
        }
    }

    private void OnDestroy()
    {
        Pools.Remove(this);
    }

    private GameObject SpawnNewPoolItem()
    {
        GameObject obj = Instantiate(Prefab);
        obj.transform.parent = transform;
        if (UseFixedParent)
        {
            obj.transform.SetParent(FixedParent);
        }
        if (UseFixedLayer)
        {
            obj.layer = FixedLayer;
        }

        obj.transform.localPosition = Vector3.zero;
        obj.transform.localRotation = Quaternion.identity;
        obj.transform.localScale = Prefab.transform.localScale;
        obj.name = string.IsNullOrEmpty(PoolItemName) ? name : PoolItemName;
        obj.SetActive(false);
        _pooledObjects.Add(obj);
        UpdateEditor();
        return obj;
    }

    private void UpdateEditor()
    {
#if UNITY_EDITOR
        EditorUtility.SetDirty(this);
#endif
    }

    private void Grow()
    {
        for (int i = 0; i < GrowAmount; i++)
        {
            SpawnNewPoolItem();
        }
    }

    public GameObject Spawn(Vector3 position, Quaternion? rotation = null, Vector3? scale = null)
    {
        if (IsFull)
        {
            if (Mode == ObjectPoolMode.FixedSize)
            {
                Debug.LogError(string.Format("Attempted to spawn from a fixed-sized object pool that is full (PoolName: {0})", name));
                return null;
            }
            Grow();
        }

        GameObject obj = _pooledObjects.First(g => !g.activeInHierarchy);
        obj.transform.position = position;
        obj.transform.rotation = rotation.HasValue ? rotation.Value : Quaternion.identity;
        obj.transform.localScale = scale.HasValue ? scale.Value : Vector3.one;
        obj.SetActive(true);
        UpdateEditor();
        OnSpawn.Invoke(obj);
        return obj;
    }

    public void Recycle(GameObject objectInstance)
    {
        if (objectInstance != null && _pooledObjects.Any(g => g == objectInstance))
        {
            objectInstance.SetActive(false);
            OnRecycle.Invoke(objectInstance);
            UpdateEditor();
        }
    }

    public static void RecycleObject(GameObject objectInstance)
    {
        if (objectInstance != null)
        {
            objectInstance.SetActive(false);
            // TODO: Find pool of object and call UpdateEditor
        }
    }
}
