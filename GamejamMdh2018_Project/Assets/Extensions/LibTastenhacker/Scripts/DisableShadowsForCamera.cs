﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class DisableShadowsForCamera : MonoBehaviour
{
    private float _cachedShadowDistance;

    public void OnPreRender()
    {
        _cachedShadowDistance = QualitySettings.shadowDistance;
        QualitySettings.shadowDistance = float.Epsilon;
    }

    public void OnPostRender()
    {
        QualitySettings.shadowDistance = _cachedShadowDistance;
    }
}
