﻿using UnityEngine;
using Tastenhacker.Core.Manager;

public class ManagerBehaviour : MonoBehaviour
{
    protected T Get<T>() where T : ManagerModule
    {
        return Manager.Instance.Get<T>();
    }
}
