﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

using UnityEngine;

namespace Tastenhacker.Scripts
{
    public class MimicCamera : MonoBehaviour
    {
        public Camera TemplateCamera;

        private void LateUpdate()
        {
            transform.rotation = TemplateCamera.transform.rotation;
        }
    }
}