
#if NGUI
/// <summary>
/// Stretches a GameObjects scale to match the screen height and/or width
/// </summary>
/// <remarks>This script is executed in EditMode</remarks>
[ExecuteInEditMode]
public class StretchToScreenSize : MonoBehaviour 
{
	/// <summary>
	/// If true, the gameObject will be scaled to screen height along its Y-scale
	/// </summary>
	public bool StretchToHeight = false;

	/// <summary>
	/// If true, the gameObject will be scaled to screen width along its X-scale
	/// </summary>
	public bool StretchToWidth = false;

	public float AdditionalHeight;
	public float AdditionalWidth;

    public bool KeepAspecRatio;
    public bool AdaptToZoom;

    public Vector2 InitialSize;

	void Update() 
	{
		if (StretchToHeight)
		{
		    if (KeepAspecRatio)
		    {
		        float ratio = InitialSize.x/InitialSize.y;
		        float newHeight = Screen.height + AdditionalHeight;
		        float newWidth = newHeight * ratio;
		        if (AdaptToZoom)
		        {
		            newHeight *= UIRoot.GetPixelSizeAdjustment(gameObject);
                    newWidth *= UIRoot.GetPixelSizeAdjustment(gameObject);
		        }

		        transform.localScale = new Vector3(newWidth, newHeight, transform.localScale.z);
            }
            else
                transform.localScale = new Vector3(transform.localScale.x, Screen.height + AdditionalHeight, transform.localScale.z);

		}
		if (StretchToWidth)
		{
			transform.localScale = new Vector3(Screen.width + AdditionalWidth, transform.localScale.y, transform.localScale.z);
		}
	}
}
#endif