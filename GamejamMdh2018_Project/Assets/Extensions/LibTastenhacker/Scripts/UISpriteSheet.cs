﻿
#if NGUI
[RequireComponent(typeof(UI2DSprite))]
public class UISpriteSheet : MonoBehaviour
{
	public float FPS = 1;

	private UI2DSprite _sprite;
	private int _index;
	public Sprite[] Frames;

	void Start ()
	{
		_sprite = GetComponent<UI2DSprite>();
		_index = 0;
		StartAnimation();
	}

	public void StartAnimation()
	{
		StartCoroutine(Animate());
	}

	public void StopAnimation()
	{
		StopCoroutine(Animate());
	}

	private IEnumerator Animate()
	{
		while (true)
		{
			yield return new WaitForSeconds(1.0f / FPS);
			_index = (_index + 1)%Frames.Length;
			_sprite.nextSprite = Frames[_index];
		}
	}
}
#endif