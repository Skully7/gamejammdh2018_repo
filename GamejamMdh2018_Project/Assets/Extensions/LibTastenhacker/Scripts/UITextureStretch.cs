﻿
#if NGUI
public class UITextureStretch : MonoBehaviour
{
    private UITexture _tex;
    public Vector2 ReferenceSize;

	void Start ()
	{
	    _tex = GetComponent<UITexture>();
	}
	
	void Update ()
	{
	    Vector2 newSize = new Vector2();
	    float ratio = ReferenceSize.x/ReferenceSize.y;
        // Step 1: Adapt to width
	    newSize.x = Screen.width;
	    newSize.y = newSize.x / ratio;
        // Step 2: Check if height is sufficient
	    if (newSize.y < Screen.height)
	    {
	        newSize.y = Screen.height;
	        newSize.x = newSize.y * ratio;
	    }
        _tex.width = Mathf.RoundToInt(newSize.x);
	    _tex.height = Mathf.RoundToInt(newSize.y);
	}
}
#endif