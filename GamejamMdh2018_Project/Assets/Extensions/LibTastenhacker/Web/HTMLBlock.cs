﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tastenhacker.Web
{
    public class HTMLBlock
    {
        public readonly string Tag;
        public List<HTMLBlock> Children;
        public List<HTMLAttribute> Attributes; 
        public string Content;
        public bool ForceLinebreak;
        public static int Indent = 0;

        public HTMLBlock(string tag)
        {
            Tag = tag;
            Children = new List<HTMLBlock>();
            Attributes = new List<HTMLAttribute>();
        }

        protected string GetAttributeString()
        {
            return Attributes.Aggregate("", (current, attrib) => current + (" " + attrib.GetText()));
        }

        public void Write(StringBuilder writer)
        {
            if (Tag.Length == 0)
            {
                if (ForceLinebreak)
                {
                    writer.AppendLine(GetIndentation() + Content);
                }
                else
                {
                    writer.Append(GetIndentation() + Content);
                }
                return;
            }

            if (Empty)
            {
                if (ForceLinebreak)
                {
                    writer.AppendLine(GetIndentation() + string.Format("<{0}{1}", Tag, GetAttributeString()));
                }
                else
                {
                    writer.Append(string.Format("<{0}{1}", Tag, GetAttributeString()));
                }
            }
            else
            {
                writer.AppendLine(GetIndentation() + string.Format("<{0}{1}>", Tag, GetAttributeString()));
                Indent++;

                if (!string.IsNullOrEmpty(Content))
                {
                    writer.AppendLine(Content);
                }
                else
                {
                    foreach (HTMLBlock block in Children)
                    {
                        block.Write(writer);
                    }
                }
                Indent--;
            }
            writer.AppendLine(string.Format(Empty ? " />" : GetIndentation() + "</{0}>", Tag));
        }

        private bool Empty
        {
            get { return string.IsNullOrEmpty(Content) && Children.Count == 0; }
        }

        public static HTMLBlock Title(string title)
        {
            return new HTMLBlock("title") {Content = title};
        }
    
        public static HTMLBlock Image(string sourceLink)
        {
            HTMLBlock block = new HTMLBlock("img");
            block.Attributes.Add(new HTMLAttribute("src", sourceLink));
            return block;
        }

        public static string GetIndentation()
        {
            string s = "";
            for (int i = 0; i < Indent; i++)
            {
                s += "    ";
            }
            return s;
        }

        public static HTMLBlock Paragraph(string alignment = null, string style = null)
        {
            HTMLBlock block = new HTMLBlock("p");
            if(alignment != null)
                block.Attributes.Add(new HTMLAttribute("align", alignment));
            if (style != null)
                block.Attributes.Add(new HTMLAttribute("style", style));
            return block;
        }

        public static HTMLBlock Divider(string alignment = null, string style = null)
        {
            HTMLBlock block = new HTMLBlock("div");
            if (alignment != null)
                block.Attributes.Add(new HTMLAttribute("align", alignment));
            if (style != null)
                block.Attributes.Add(new HTMLAttribute("style", style));
            return block;
        }

        public static HTMLBlock Header(int size, string caption, string style = null)
        {
            HTMLBlock block = new HTMLBlock("h" + size) {Content = caption};
            if (style != null)
                block.Attributes.Add(new HTMLAttribute("style", style));
            return block;
        }

        public static HTMLBlock Span(string style = null)
        {
            HTMLBlock block = new HTMLBlock("span");
            if (style != null)
                block.Attributes.Add(new HTMLAttribute("style", style));
            return block;
        }

        public static HTMLBlock Break()
        {
            return new HTMLBlock("br");
        }

        public static HTMLBlock Static(string text, bool lineBreak = true)
        {
            return new HTMLBlock("") {Content = text, ForceLinebreak = lineBreak};
        }

        public static HTMLBlock Link(string url, string text)
        {
            HTMLBlock block = new HTMLBlock("a") { Content = text };
            block.Attributes.Add(new HTMLAttribute("href", url));
            return block;
        }

        public void AddChild(HTMLBlock htmlBlock)
        {
            Children.Add(htmlBlock);
        }
    }

    public class HTMLAttribute
    {
        public readonly string Key;
        public object Value;

        public HTMLAttribute(string key, object value)
        {
            Key = key;
            Value = value;
        }

        public string GetText()
        {
            return string.Format("{0}=\"{1}\"", Key, Value);
        }
    }
}