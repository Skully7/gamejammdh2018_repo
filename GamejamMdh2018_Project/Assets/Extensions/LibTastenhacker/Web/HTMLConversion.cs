﻿//----------------------------------------------
//          ~ Tastenhacker Library ~
//            © 2013 Thomas Hummes
//----------------------------------------------

using System.Collections.Generic;
using System.Linq;

namespace Tastenhacker.Web
{
    public static class HTMLConversion
    {
        private static readonly Dictionary<string, string> Replacements;

        static HTMLConversion()
        {
            Replacements = new Dictionary<string, string>
            {
                {"&", "&amp;"}, 
                {"ä", "&auml;"}, {"Ä", "&Auml;"},
                {"ö", "&ouml;"}, {"Ö", "&Ouml;"},
                {"ü", "&uuml;"}, {"Ü", "&Uuml;"},
                {"ß", "&szlig;"}, 
                {"€", "&euro;"},
            };
        }

        public static string EscapeChars(string input)
        {
            return Replacements.Aggregate(input, (current, entry) => current.Replace(entry.Key, entry.Value));
        }

        public static void EscapeChars(ref string input)
        {
            input = Replacements.Aggregate(input, (current, entry) => current.Replace(entry.Key, entry.Value));
        }
    }
}