﻿using UnityEngine;

public abstract class Ability : ScriptableObject
{
    protected GameObject Caster;
    protected TargetInfo Target;

    public void CastAbility(GameObject caster, TargetInfo target)
    {
        Caster = caster;
        Target = target;

        AbilityEffect();
    }

    public abstract void AbilityEffect();

    public class TargetInfo
    {
        public GameObject Target;
        private Vector3? _targetPosValue;

        public Vector3 TargetPosition
        {
            get
            {
                return _targetPosValue ?? Target.transform.position;
            }
            set { _targetPosValue = value; }
        }
    }
}
