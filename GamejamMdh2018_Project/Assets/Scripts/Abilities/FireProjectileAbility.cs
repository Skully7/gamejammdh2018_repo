﻿using System;
using UnityEngine;

[CreateAssetMenu]
public class FireProjectileAbility : Ability
{
    public Projectile ProjectilePrefab;
    public bool HitEveryone;

    public override void AbilityEffect()
    {
        Vector3 direction = (Target.TargetPosition - Caster.transform.position).normalized;
        Projectile projectile = Instantiate(ProjectilePrefab, Caster.transform.position, Quaternion.identity);

        foreach (Collider2D casterCollider in Caster.GetComponentsInChildren<Collider2D>())
        {
            Physics2D.IgnoreCollision(casterCollider, projectile.GetComponent<Collider2D>());
        }

        projectile.Initialize(direction, EvaluateCurrentTargets());
    }

    public Projectile.TargetType EvaluateCurrentTargets()
    {
        if (HitEveryone)
            return Projectile.TargetType.Everyone;

        Player p = Caster.GetComponent<Player>();
        if (p != null)
        {
            return !p.IsInsane ? Projectile.TargetType.FantasyEnemies : Projectile.TargetType.AllEnemies;
        }

        if (Caster.GetComponent<Enemy>() != null)
        {
            return Projectile.TargetType.Player;
        }

        throw new ArgumentException("Cannot evaluate targets! Caster is neither player nor enemy!");
    }
}
