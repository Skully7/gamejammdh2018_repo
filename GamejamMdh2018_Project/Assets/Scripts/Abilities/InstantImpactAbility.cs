﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu]
public class InstantImpactAbility : Ability
{
    public InstantImpact ImpactPrefab;
    public bool HitEveryone;
    public float MaxRandomCenterOffset;

    public override void AbilityEffect()
    {
        Vector3 impactPosition = new Vector3(Target.TargetPosition.x, Target.TargetPosition.y, 0);
        if (MaxRandomCenterOffset > 0)
            impactPosition += (Quaternion.Euler(0, 0, Random.Range(0, 360)) * Vector2.up).normalized *
                             Random.Range(0, MaxRandomCenterOffset);
        InstantImpact impact = Instantiate(ImpactPrefab, impactPosition, Quaternion.identity);

        foreach (Collider2D casterCollider in Caster.GetComponentsInChildren<Collider2D>())
        {
            Physics2D.IgnoreCollision(casterCollider, impact.GetComponent<Collider2D>());
        }

        impact.Initialize(EvaluateCurrentTargets());
    }

    public Projectile.TargetType EvaluateCurrentTargets()
    {
        if (HitEveryone)
            return Projectile.TargetType.Everyone;

        Player p = Caster.GetComponent<Player>();
        if (p != null)
        {
            return !p.IsInsane ? Projectile.TargetType.FantasyEnemies : Projectile.TargetType.AllEnemies;
        }

        if (Caster.GetComponent<Enemy>() != null)
        {
            return Projectile.TargetType.Player;
        }

        throw new ArgumentException("Cannot evaluate targets! Caster is neither player nor enemy!");
    }
}
