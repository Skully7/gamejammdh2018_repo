﻿using UnityEngine;

[CreateAssetMenu]
public class PlayerAbility : ScriptableObject
{
    public string DisplayName;
    public Sprite Icon;
    public float MaxCooldown;
    public float MinCooldown;

    public Ability AbilityObject;

    public void CastAbility(GameObject caster, Ability.TargetInfo target)
    {
        AbilityObject.CastAbility(caster, target);
    }
}
