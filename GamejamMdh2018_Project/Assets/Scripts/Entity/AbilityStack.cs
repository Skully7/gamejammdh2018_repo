﻿using UnityEngine;

public class AbilityStack : MonoBehaviour
{
    public Ability[] Abilities;

    public void TriggerAbility(int index, Ability.TargetInfo target)
    {
        Abilities[index].CastAbility(gameObject, target);
    }
}
