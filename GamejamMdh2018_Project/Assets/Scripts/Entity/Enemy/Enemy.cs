﻿using UnityEngine;
using UnityEngine.AI;
using System.Linq;

[RequireComponent(typeof(RedBlinkDmgEffect))]
public class Enemy : MonoBehaviour, IDamageable
{
    public virtual bool IsFantasyEnemy => false;

    public int StartHp = 50;
    private int _hpValue;

    public SpriteRenderer Sprite;

    [SerializeField]
    private GameObject[] _waypoints;
    private int _waypointIndex;
    protected NavMeshAgent _agent;

    private Animator _animator;
    private GameObject _fov;
    private FieldOfView _fovs;

    private RedBlinkDmgEffect _dmgEffect;

    public int Hp
    {
        get { return _hpValue; }
        private set
        {
            bool changed = _hpValue != value;

            _hpValue = Mathf.Clamp(value, 0, StartHp);

            if(changed)
                OnHpChanged();
            if (_hpValue <= 0)
                OnDeath();
        }
    }

    protected virtual void Awake()
    {
        _dmgEffect = GetComponent<RedBlinkDmgEffect>();
    }

    protected virtual void Start ()
	{
	    Hp = StartHp;
        _agent = GetComponent<NavMeshAgent>();
        _agent.updateRotation = false;
        _animator = GetComponentInChildren<Animator>();
        _fov = GetComponentInChildren<MeshRenderer>()?.gameObject;
        if(_fov != null)
            _fovs = _fov.GetComponent<FieldOfView>();

        if (_waypoints.Any())
            transform.position = _waypoints[0].transform.position;
    }

    #region Testing

    protected virtual void Update()
    {
        GameObject player = _fovs?.FindVisibleTargets();

        if (player)
        {
            _waypointIndex = -1;
            _agent.SetDestination(player.transform.position);
        }

        if (_agent.remainingDistance < 0.2)
            _waypointIndex = 0;

        if (_waypoints.Any())
            if (_waypointIndex >= 0)
            {
                Vector3 targetLoc = _waypoints[_waypointIndex].transform.position;
                if (!_agent.hasPath || Vector2.Distance(transform.position, targetLoc) < 1)
                {
                    _waypointIndex = (_waypointIndex + 1) % _waypoints.Length;
                    _agent.SetDestination(_waypoints[_waypointIndex].transform.position);
                }
            }

        Vector2 vel = _agent.velocity;

        if (_animator != null && !IsFantasyEnemy)
        {
            _animator.SetFloat("Horizontal", vel.x);
            _animator.SetFloat("Vertical", vel.y);
            if (_fov != null)
            {
                string name = _animator.GetCurrentAnimatorClipInfo(0).OrderByDescending(aci => aci.weight).First().clip.name;
                _fov.transform.localRotation = Quaternion.Euler(0f, name.Contains("North") ? 0f : name.Contains("South") ? 180f : name.Contains("West") ? 90f : 270f, 0f);
            }
        } else if(_fov != null)
            _fov.transform.localRotation = Quaternion.Euler(0f, vel.y > 0.2f ? 0f : vel.y < -0.2f ? 180f : vel.x < -0.2f ? 90f:270f, 0f);
    }

    #endregion

    public virtual void TakeDamage(int dmg)
    {
        Hp -= dmg;
        _dmgEffect.Play();
    }

    private void OnHpChanged()
    {
        //TODO For testing. Remove later
        if(Sprite != null)
            Sprite.color = Color.Lerp(Color.red, Color.white, (float)_hpValue / StartHp);
    }

    protected virtual void OnDeath()
    {
        Destroy(gameObject);
    }
}
