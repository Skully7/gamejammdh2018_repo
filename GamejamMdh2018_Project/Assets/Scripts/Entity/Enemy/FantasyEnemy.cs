﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Movement))]
public class FantasyEnemy : Enemy
{
    public override bool IsFantasyEnemy => true;

    public float MovementSpeed = 1;
    public bool CanPassObstacles;

    public InsanatyDropSettings InsanitySettings;

    public float ApproachDistance = 10f;
    public FantasyEnemyAction[] Actions;

    public event Action<FantasyEnemy> OnDied;

    public Ability[] Abilities;

    private readonly Dictionary<FantasyEnemyAction, float> _cooldowns = new Dictionary<FantasyEnemyAction, float>();

    private Player _player;

    private Movement _movement;

    public LayerMask VisionOnstacles;

    private Animator _anim;

    protected override void Awake()
    {
        base.Awake();

        _movement = GetComponent<Movement>();
        _anim = GetComponentInChildren<Animator>();
    }

    protected override void Start()
    {
        base.Start();

        if (CanPassObstacles)
        {
            gameObject.layer = LayerMask.NameToLayer("PassWalls");
        }

        foreach (FantasyEnemyAction fea in Actions)
        {
            _cooldowns.Add(fea, Random.Range(fea.CooldownRangeMin, fea.CooldownRangeMax) * .5f);
        }

        _player = FindObjectOfType<Player>();
    }

    protected override void Update()
    {
        base.Update();

        ProcessMovement();
        ProcessActions();
    }


    private void ProcessMovement()
    {
        if (Vector3.Distance(_player.transform.position, transform.position) > ApproachDistance)
        {
            if (CanPassObstacles)
                _movement.Move((_player.transform.position - transform.position).normalized * MovementSpeed); //TODO Use special collision-layer here
            else
                _agent.SetDestination(_player.transform.position);
        }
    }

    private void ProcessActions()
    {
        foreach (FantasyEnemyAction fea in Actions)
        {
            if (_cooldowns[fea] > 0)
                _cooldowns[fea] -= Time.deltaTime;
            else
                _cooldowns[fea] = 0;

            if (_cooldowns[fea] <= 0)
            {
                if (fea.MinDistance <= 0 || fea.MinDistance >= Vector3.Distance(transform.position, _player.transform.position))
                {
                    if (!fea.NeedsVision || CheckVisionToPlayer())
                    {
                        //TODO Check for burst?

                        if (fea.DirectDamage > 0)
                            _player.TakeDamage(fea.DirectDamage);
                        if (fea.AbilityIndex >= 0)
                        {
                            if (Random.Range(0, 1f) <= fea.Burst.BurstChance)
                                StartCoroutine(Burst(fea));
                            else
                                Abilities[fea.AbilityIndex].CastAbility(gameObject, new Ability.TargetInfo { TargetPosition = _player.transform.position });
                        }

                        if(!string.IsNullOrWhiteSpace(fea.AnimationTrigger) && _anim != null)
                            _anim.SetTrigger(fea.AnimationTrigger);

                        _cooldowns[fea] += Random.Range(fea.CooldownRangeMin, fea.CooldownRangeMax);
                    }
                }
            }
        }
    }

    private IEnumerator Burst(FantasyEnemyAction action)
    {
        for (int i = 0; i < action.Burst.BurstRate; i++)
        {
            Abilities[action.AbilityIndex].CastAbility(gameObject, new Ability.TargetInfo { TargetPosition = _player.transform.position });
            yield return new WaitForSeconds(action.Burst.BurstDelay);
        }
    }


    public override void TakeDamage(int dmg)
    {
        base.TakeDamage(dmg);

        if (InsanitySettings.DamageInsanatySmall > 0 || InsanitySettings.DamageInsanatyLarge > 0)
            InsanityDropSpawner.Instance.Spawn(transform.position, InsanitySettings.DamageInsanatySmall, InsanitySettings.DamageInsanatyLarge);
    }

    protected override void OnDeath()
    {
        if (InsanitySettings.KillInsanitySmall > 0 || InsanitySettings.KillInsanityLarge > 0)
            InsanityDropSpawner.Instance.Spawn(transform.position, InsanitySettings.KillInsanitySmall, InsanitySettings.KillInsanityLarge);

        OnDied?.Invoke(this);

        base.OnDeath();
    }


    private bool CheckVisionToPlayer()
    {
        RaycastHit2D hit = Physics2D.Linecast(transform.position + (_player.transform.position - transform.position).normalized, _player.transform.position, VisionOnstacles);

        if (hit.collider == null)
            return false;
        return hit.collider.GetComponent<Player>() != null;
    }


    [System.Serializable]
    public class InsanatyDropSettings
    {
        public int DamageInsanatySmall = 1;
        public int DamageInsanatyLarge = 0;
        public int KillInsanitySmall = 3;
        public int KillInsanityLarge = 1;
    }

    [System.Serializable]
    public class FantasyEnemyAction
    {
        public bool NeedsVision;
        public float MinDistance = -1;

        public float CooldownRangeMin;
        public float CooldownRangeMax;
        public BurstSettings Burst = new BurstSettings();

        public int AbilityIndex = -1;
        public int DirectDamage;

        public string AnimationTrigger;
    }

    [System.Serializable]
    public class BurstSettings
    {
        [Range(0, 1f)]
        public float BurstChance = .1f;
        public float BurstDelay = .75f;
        public int BurstRate = 3;
    }
}
