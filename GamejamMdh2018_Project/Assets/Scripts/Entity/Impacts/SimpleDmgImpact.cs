﻿public class SimpleDmgImpact : InstantImpact
{
    public int Damage = 35;

    public override void OnHit(IDamageable hitObject)
    {
        hitObject.TakeDamage(Damage);
    }
}
