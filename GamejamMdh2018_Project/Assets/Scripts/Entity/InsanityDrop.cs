﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class InsanityDrop : MonoBehaviour
{
    public static Vector2 ForceMinMax = new Vector2(1000,3000f);
    private const float AproachForceMod = 450;

    private int _insanityGain;

    private Rigidbody2D _rb;

    private Transform _target;

    private float _forceGrowDelay = 5;
    private float _lifetime;

    protected virtual void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    protected virtual void Start()
    {
        RigidbodySetup();
    }

    protected virtual void Update()
    {
        _lifetime += Time.deltaTime;
        float bonusForce = _lifetime > _forceGrowDelay ? _lifetime - _forceGrowDelay : 0;
        _rb.AddForce(
            (_target.transform.position - transform.position).normalized * (AproachForceMod + bonusForce) 
                     / Mathf.Max(Vector3.Distance(_target.transform.position, transform.position), 0.1f)
            );
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            p.Insanity += _insanityGain;
            Destroy(gameObject);
        }
    }

    public void Initialize(Transform target, int insanityGain)
    {
        _target = target;
        _insanityGain = insanityGain;

        _rb.AddForce(Quaternion.Euler(0,0,Random.Range(0,360)) * Vector2.up * Random.Range(ForceMinMax.x, ForceMinMax.y), ForceMode2D.Force);
    }

    private void RigidbodySetup()
    {
        if (_rb.gravityScale > 0)
        {
            Debug.Log($"[InsanityDrop] The '{gameObject.name}' rigidbodys gravity-scall will be set to 0.");
            _rb.gravityScale = 0;
        }
    }
}
