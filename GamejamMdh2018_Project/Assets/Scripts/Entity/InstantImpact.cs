﻿using System.Collections;
using UnityEngine;

public abstract class InstantImpact : MonoBehaviour
{
    public float Lifetime = 2;
    public float Range;

    public float TriggerDelay;

    protected Projectile.TargetType Targets { get; private set; }
    private WaitForSeconds _wait;

    private static bool _suppressParticleEmission;
    private float _timer;

    protected virtual void Awake()
    {
        _wait = new WaitForSeconds(TriggerDelay);
    }

    protected virtual void Start()
    {
        if (TriggerDelay <= 0)
            CalculateImpact();
        else
            StartCoroutine(CalculateImpactDelayed());
    }

    protected virtual void OnEnable()
    {
        Application.quitting += SuppressSpawningOnExit;
    }

    protected virtual void OnDisable()
    {
        Application.quitting -= SuppressSpawningOnExit;
    }

    protected virtual void Update()
    {
        if (Lifetime <= 0)
            return;

        _timer += Time.deltaTime;
        if (_timer >= Lifetime)
        {
            Destroy(gameObject);
        }
    }

    protected virtual void OnDestroy()
    {
        if (!_suppressParticleEmission)
            OnDeath();
    }


    private IEnumerator CalculateImpactDelayed()
    {
        yield return _wait;
        CalculateImpact();
    }

    private void CalculateImpact()
    {
        Collider2D[] colliderInRange = Physics2D.OverlapCircleAll(transform.position, Range);

        foreach (Collider2D c in colliderInRange)
        {
            CallMethodForTarget(c);
        }
    }

    private void CallMethodForTarget(Collider2D target)
    {
        OnCollision(target);
        if (Projectile.IsValidTarget(target, Targets))
        {
            OnHit(target.gameObject.GetComponent<IDamageable>());
        }
    }


    /// <summary>
    /// Is called, when the impact dies (after timeout)
    /// </summary>
    public virtual void OnDeath() { }

    /// <summary>
    /// Is called, when the impact does hit anything.
    /// </summary>
    /// <param name="hitCollider">The collider that was hit</param>
    public virtual void OnCollision(Collider2D hitCollider) { }

    /// <summary>
    /// Is called, when the impact does hit any assigned target.
    /// </summary>
    /// <param name="hitObject">The target (needs to be IDamageable) that was hit</param>
    public virtual void OnHit(IDamageable hitObject) { }

    public void Initialize(Projectile.TargetType targets)
    {
        Targets = targets;
    }

    private static void SuppressSpawningOnExit()
    {
        _suppressParticleEmission = true;
    }
}
