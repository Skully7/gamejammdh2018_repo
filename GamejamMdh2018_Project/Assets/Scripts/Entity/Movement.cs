using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour
{
    public const float SpeedModConstant = 10;
    public const float LinearDragConstant = 150;

    public float SpeedMod = 1;

    private Rigidbody2D _rb;
    private Animator _anim;

    protected virtual void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponentInChildren<Animator>();
    }

    protected virtual void Start()
    {
        RigidbodySetup();
    }

    public void Move(Vector2 move)
    {
        _rb.AddForce(move * SpeedModConstant * SpeedMod, ForceMode2D.Impulse);

        if (_anim == null)
            return;

        _anim.SetFloat("Horizontal", move.x);
        _anim.SetFloat("Vertical", move.y);
    }

    private void RigidbodySetup()
    {
        if (Math.Abs(_rb.drag - LinearDragConstant) > Mathf.Epsilon)
        {
            _rb.drag = LinearDragConstant;
            Debug.Log($"[Movement] '{gameObject.name}'s Rigidbody2d.drag was set to {LinearDragConstant}!");
        }

        if (Math.Abs(_rb.gravityScale - 0) > Mathf.Epsilon)
        {
            _rb.gravityScale = 0;
            Debug.Log($"[Movement] '{gameObject.name}'s Rigidbody2d.gravityScale was set to 0!");
        }
    }
}
