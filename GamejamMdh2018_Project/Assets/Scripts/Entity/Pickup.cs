﻿using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        Player p = other.GetComponent<Player>();
        if (p != null)
        {
            if(OnPickup(p))
                Destroy(gameObject);
        }
    }

    protected abstract bool OnPickup(Player player);
}
