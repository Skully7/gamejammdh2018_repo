﻿public class DrugsPickup : Pickup
{
    public int InsanityGain = 35;

    protected override bool OnPickup(Player player)
    {
        if (player.IsInsane)
            return false;

        player.Insanity += InsanityGain;
        return true;
    }
}
