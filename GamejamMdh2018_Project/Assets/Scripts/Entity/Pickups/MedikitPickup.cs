﻿public class MedikitPickup : Pickup
{
    public int HealAmount = 25;

    protected override bool OnPickup(Player player)
    {
        if (player.Hp >= Player.MaxHp)
            return false;

        player.Hp += HealAmount;
        return true;
    }
}
