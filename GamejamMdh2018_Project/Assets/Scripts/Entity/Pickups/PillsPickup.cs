﻿public class PillsPickup : Pickup
{
    public int InsanityLoss = 50;

    protected override bool OnPickup(Player player)
    {
        if (player.Insanity <= 0 || player.IsInsane)
            return false;

        player.Insanity -= InsanityLoss;
        return true;
    }
}
