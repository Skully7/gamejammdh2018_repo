using System;
using System.Collections.Generic;
using Tastenhacker.Core.Manager;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Movement), typeof(RedBlinkDmgEffect))]
public class Player : MonoBehaviour, IDamageable
{
    public const int MaxHp = 100;
    public const int MaxInsanity = 100;

    public event Action OnHealthChanged;
    public event Action OnInsanityChanged;
    public event Action OnCurrentSkillChanged;
    public event Action<bool> OnInsanityStateChanged;

    private FX _fx;

    [SerializeField] private int _hp = MaxHp;
    public int Hp
    {
        get
        {
            return _hp;
        }
        set
        {
            bool changed = value != _hp;

            if (value < _hp)
                _regBockTimer = HealthStats.RegBockTime;

            _hp = Mathf.Clamp(value, 0, MaxHp);

            if (changed)
            {
                OnHealthChanged?.Invoke();
                _fx.VignetteLevel = Mathf.Clamp01(1 - (float) value / MaxHp);
            }

            if (value <= 0)
            {
                OnDeath?.Invoke();
                Time.timeScale = 0;
            }
        }
    }

    [SerializeField] private int _insanity;
    public int Insanity
    {
        get
        {
            return _insanity;
        }
        set
        {
            bool changed = value != _insanity;

            if (value > _insanity && _burnoutTimer <= 0)
                ResetInsanityFalloffBlock();

            _insanity = Mathf.Clamp(value, 0, MaxInsanity);

            if (changed && _insanity >= MaxInsanity && _burnoutTimer <= 0)
            {
                _insanityTimer = InsanityStats.BurnoutDuration;
            }

            if (changed)
                OnInsanityChanged?.Invoke();
        }
    }

    public HealthSettings HealthStats;
    public InsanitySettings InsanityStats;
    

    public bool IsInsane => Insanity >= MaxInsanity;
    public bool IsInBurnout => _burnoutTimer > 0;

    [Range(0.1f,5)]
    public float MovementSpeed = 1;

    public AbilityCastInfo[] CastInfos;

    private int _currentSelectedValue;

    private readonly Dictionary<AbilityCastInfo, float> _cooldowns = new Dictionary<AbilityCastInfo, float>();
    private readonly Dictionary<AbilityCastInfo, bool> _abilityStates = new Dictionary<AbilityCastInfo, bool>();

    public int CurrentSelectedAbility
    {
        get
        {
            return _currentSelectedValue;
        }
        private set
        {
            bool changed = _currentSelectedValue != value;
                
            _currentSelectedValue = value;

            if(changed)
                OnCurrentSkillChanged?.Invoke();
        }
    }

    public UnityEvent OnDeath;

    private Movement _move;
    private RedBlinkDmgEffect _dmgEffect;

    private float _decreaseNextInsanityTimer = 0;
    private float _decreaseDelayTimer = 0;
    private float _insanityTimer;
    private float _burnoutTimer;

    private float _regBockTimer;
    private float _regRateTimer;

    private bool _wasInsaneLastFrame;

    protected virtual void Awake()
    {
        _move = GetComponent<Movement>();
        _dmgEffect = GetComponent<RedBlinkDmgEffect>();
    }

    protected virtual void Start()
    {
        _fx = Manager.Instance.Get<FX>();
        OnInsanityChanged?.Invoke();

        foreach (AbilityCastInfo castInfo in CastInfos)
        {
            _cooldowns.Add(castInfo, 0);
            _abilityStates.Add(castInfo, castInfo.MinInsanity <= Insanity);
        }
    }

    protected virtual void Update()
    {
        HandleInput();
        HandleHealthStats();
        HandleInsanityStats();
        ProcessCooldownsAndStates();

        if (IsInsane != _wasInsaneLastFrame)
        {
            OnInsanityStateChanged?.Invoke(IsInsane);
        }
        _wasInsaneLastFrame = IsInsane;

        //if (Input.GetKeyDown(KeyCode.Space))
        //    Insanity += 20;
    }

    private void HandleInput()
    {
        //Move
        Vector2 moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        _move.Move(moveInput.normalized * MovementSpeed);
        //Abilities
        //  Selection (Keyboard)
        for (int i = 0; i < CastInfos.Length; i++)
        {
            AbilityCastInfo castInfo = CastInfos[i];

            if (_abilityStates[castInfo])
            {
                if (Input.GetKeyDown(castInfo.TriggerKey))
                {
                    CurrentSelectedAbility = i;
                }
            }
        }

        //  Selection (Mousewheel)
        float mousewheel = Input.GetAxis("Mouse ScrollWheel");
        if (mousewheel < 0)
        {
            CurrentSelectedAbility++;
            if (CurrentSelectedAbility >= CastInfos.Length)
                CurrentSelectedAbility = 0;

            while (!_abilityStates[CastInfos[CurrentSelectedAbility]])
            {
                CurrentSelectedAbility++;
                if (CurrentSelectedAbility >= CastInfos.Length)
                    CurrentSelectedAbility = 0;
            }
        }
        if (mousewheel > 0)
        {
            CurrentSelectedAbility--;
            if (CurrentSelectedAbility < 0)
                CurrentSelectedAbility = CastInfos.Length - 1;

            while (!_abilityStates[CastInfos[CurrentSelectedAbility]])
            {
                CurrentSelectedAbility--;
                if (CurrentSelectedAbility < 0)
                    CurrentSelectedAbility = CastInfos.Length - 1;
            }
        }
        //  Fire
        if (Input.GetMouseButtonDown(0))
        {
            if(_cooldowns[CastInfos[CurrentSelectedAbility]] > 0)
                return;

            CastInfos[CurrentSelectedAbility].AbilityObject.CastAbility(gameObject, GetWeakTargetInfo());
            _cooldowns[CastInfos[CurrentSelectedAbility]] = CalculateCurrentCooldown(CastInfos[CurrentSelectedAbility]);
        }
    }


    private float CalculateCurrentCooldown(AbilityCastInfo castInfo)
    {
        float lAlpha = Mathf.Clamp01(Mathf.InverseLerp(castInfo.MinInsanity, MaxInsanity, Insanity));
        return Mathf.Lerp(castInfo.AbilityObject.MaxCooldown, castInfo.AbilityObject.MinCooldown, lAlpha);
    }


    private void HandleHealthStats()
    {
        if (_regBockTimer > 0)
            _regBockTimer -= Time.deltaTime;
        else
            _regBockTimer = 0;

        if (_regBockTimer <= 0)
        {
            _regRateTimer -= Time.deltaTime;
            if (_regRateTimer <= 0)
            {
                _regRateTimer = IsInsane ? HealthStats.InsanityRegenerationRate : HealthStats.RegenerationRate;
                Hp++;
            }
        }
    }

    private void HandleInsanityStats()
    {
        if (_insanityTimer > 0)
        {
            _insanityTimer -= Time.deltaTime;

            if(_insanityTimer <= 0)
            {
                _insanityTimer = 0;
                _burnoutTimer = InsanityStats.BurnoutDuration;
            }
            return;
        }

        if (_burnoutTimer > 0)
        {
            _burnoutTimer -= Time.deltaTime;
        }

        if (_decreaseDelayTimer > 0 && _burnoutTimer <= 0)
        {
            _decreaseDelayTimer -= Time.deltaTime;
            _decreaseNextInsanityTimer = 0;
            return;
        }

        _decreaseNextInsanityTimer += Time.deltaTime;
        if (_decreaseNextInsanityTimer > (_burnoutTimer > 0 ? InsanityStats.DecreaseSecondsBurnout : InsanityStats.DecreaseSeconds))
        {
            _decreaseNextInsanityTimer -= _burnoutTimer > 0 ? InsanityStats.DecreaseSecondsBurnout : InsanityStats.DecreaseSeconds;

            Insanity--;
        }
    }

    private void ProcessCooldownsAndStates()
    {
        foreach (AbilityCastInfo castInfo in CastInfos)
        {
            if (_cooldowns[castInfo] > 0)
                _cooldowns[castInfo] -= Time.deltaTime;
            else
                _cooldowns[castInfo] = 0;

            _abilityStates[castInfo] = castInfo.MinInsanity <= Insanity;
        }
    }


    private void ResetInsanityFalloffBlock()
    {
        _decreaseDelayTimer = InsanityStats.DecreaseDelay;
    }


    public Ability.TargetInfo GetWeakTargetInfo()
    {
        return new Ability.TargetInfo
        {
            TargetPosition =
                Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0))
        };
    }

    public void TakeDamage(int dmg)
    {
        Hp -= dmg;
        _dmgEffect.Play();
    }

    public float GetCooldownAmount(int abilityIndex)
    {
        return Mathf.Clamp01(_cooldowns[CastInfos[abilityIndex]] / CalculateCurrentCooldown(CastInfos[abilityIndex]));
    }

    public bool GetAbilityState(int abilityIndex)
    {
        return _abilityStates[CastInfos[abilityIndex]];
    }


    [Serializable]
    public class AbilityCastInfo
    {
        public PlayerAbility AbilityObject;
        public KeyCode TriggerKey;

        [Range(0,100)]
        public int MinInsanity;
    }

    [Serializable]
    public class InsanitySettings
    {
        public float DecreaseSeconds = 5;
        public float DecreaseDelay = 10;
        public float InsanityPowerphaseDuration = 30;
        public float DecreaseSecondsBurnout = .1f;
        public float BurnoutDuration = 10;
    }

    [Serializable]
    public class HealthSettings
    {
        public float RegenerationRate = 3;
        public float InsanityRegenerationRate = 0.25f;
        public float RegBockTime = 5;
    }
}
