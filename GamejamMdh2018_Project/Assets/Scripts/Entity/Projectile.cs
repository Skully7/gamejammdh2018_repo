﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public abstract class Projectile : MonoBehaviour
{
    public float Lifetime = 5;
    public float Speed = 5;

    public bool DestroyOnObstacles = true;
    public bool DestroyOnTargets = true;

    public float CollisionSpawnDelay;

    protected TargetType Targets { get; private set; }

    private float _timer;
    private Rigidbody2D _rb;

    private Vector2? _lastVelocity;
    protected Vector2 Direction => _lastVelocity??_rb.velocity.normalized;
    protected float MovementSpeed => _rb.velocity.magnitude;

    private Collider2D _collider;
    private WaitForSeconds _collReenableDelay;

    private static bool _suppressParticleEmission;

    protected virtual void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
    }

    protected virtual void Start()
    {
        RigidbodySetup();
        if (CollisionSpawnDelay > 0)
        {
            _collReenableDelay = new WaitForSeconds(CollisionSpawnDelay);
            _collider.enabled = false;

            StartCoroutine(ReenableColliderDelayed());
        }
    }

    protected virtual void OnEnable()
    {
        Application.quitting += SuppressSpawningOnExit;
    }

    protected virtual void OnDisable()
    {
        Application.quitting -= SuppressSpawningOnExit;
    }

    protected virtual void Update()
    {
        if(Lifetime <= 0)
            return;

        _timer += Time.deltaTime;
        if (_timer >= Lifetime)
        {
            Destroy(gameObject);
        }
    }

    protected virtual void OnDestroy()
    {
        if(!_suppressParticleEmission)
            OnDeath();
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        _lastVelocity = _rb.velocity;
        OnCollision(other);
        if (IsValidTarget(other, Targets))
        {
            OnHit(other.gameObject.GetComponent<IDamageable>());

            if(DestroyOnTargets)
                Destroy(gameObject);
        }
        else if (DestroyOnObstacles && other.GetComponent<IDamageable>() == null)
        {
            Destroy(gameObject);
        }
    }


    /// <summary>
    /// Is called, when the projectile dies (after timeout, collision or hit)
    /// </summary>
    public virtual void OnDeath() { }

    /// <summary>
    /// Is called, when the projectile does hit anything.
    /// </summary>
    /// <param name="hitCollider">The collider that was hit</param>
    public virtual void OnCollision(Collider2D hitCollider) { }

    /// <summary>
    /// Is called, when the projectile does hit any assigned target.
    /// </summary>
    /// <param name="hitObject">The target (needs to be IDamageable) that was hit</param>
    public virtual void OnHit(IDamageable hitObject) { }


    public void Initialize(Vector2 direction, TargetType targets, float speedMod = 1)
    {
        _rb.velocity = direction.normalized * Speed * speedMod;
        Targets = targets;

        _lastVelocity = _rb.velocity;
    }

    private void RigidbodySetup()
    {
        if (Math.Abs(_rb.gravityScale) > Mathf.Epsilon)
        {
            Debug.Log($"[Projectile] Sets up '{gameObject.name}' rigidbodies gravity-scale to 0.");
            _rb.gravityScale = 0;
        }
    }

    private IEnumerator ReenableColliderDelayed()
    {
        yield return _collReenableDelay;
        _collider.enabled = true;
    }


    // ReSharper disable once SuggestBaseTypeForParameter
    public static bool IsValidTarget(Collider2D c, TargetType target)
    {
        if (c.gameObject.GetComponent<IDamageable>() == null)
            return false;

        if (c.gameObject.GetComponent<Player>() && (target == TargetType.Player || target == TargetType.Everyone))
            return true;

        Enemy e = c.gameObject.GetComponent<Enemy>();
        if (e == null)
            return false;

        if(e.IsFantasyEnemy)
            return target == TargetType.FantasyEnemies || target == TargetType.AllEnemies || target == TargetType.Everyone;

        return target == TargetType.AllEnemies || target == TargetType.Everyone;
    }

    private static void SuppressSpawningOnExit()
    {
        _suppressParticleEmission = true;
    }


    public enum TargetType
    {
        Player, FantasyEnemies, AllEnemies, Everyone
    }

    public static Vector2 RotateVector(Vector2 v, float angle)
    {
        return Quaternion.Euler(0, 0, angle) * v;
    }
}
