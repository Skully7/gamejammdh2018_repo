﻿using UnityEngine;

public class AngledSubemissionProjectile : Projectile
{
    public Projectile SubEmissionProjectile;
    public int NumberOfSubemission = 6;

    [Range(1,360)]
    public float Angle = 90;

    private bool _supressSubemission;

    public override void OnCollision(Collider2D hitCollider)
    {
        _supressSubemission = true;
        SpawnSubemission(-Direction);
    }

    public override void OnDeath()
    {
        if(!_supressSubemission)
            SpawnSubemission(Direction);
    }

    private void SpawnSubemission(Vector2 mainDirection)
    {
        Vector2 startAngle = RotateVector(mainDirection, -(Angle / 2));
        for (int i = 0; i < NumberOfSubemission; i++)
        {
            Vector2 sumEmissionDirection = RotateVector(startAngle, Angle / NumberOfSubemission * i);

            Projectile subProjectile = Instantiate(SubEmissionProjectile, transform.position, Quaternion.identity);
            subProjectile.Initialize(sumEmissionDirection, Targets);
        }
    }
}
