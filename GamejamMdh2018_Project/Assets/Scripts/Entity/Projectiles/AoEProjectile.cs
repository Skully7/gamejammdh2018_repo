﻿using UnityEngine;

public class AoEProjectile : Projectile
{
    public int Damage = 10;
    public float Range = 3;

    public GameObject IndicatorPrefab;

    public override void OnCollision(Collider2D hitCollider)
    {
        GameObject indicator = Instantiate(IndicatorPrefab, transform.position, Quaternion.identity);
        indicator.transform.localScale = Vector3.one * Range;

        Collider2D[] inRange = Physics2D.OverlapCircleAll(transform.position, Range);
        foreach (Collider2D coll in inRange)
        {
            coll.GetComponent<IDamageable>()?.TakeDamage(Damage);
        }
    }
}
