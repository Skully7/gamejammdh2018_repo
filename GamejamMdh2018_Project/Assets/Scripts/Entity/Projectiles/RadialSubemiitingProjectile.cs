﻿using UnityEngine;

public class RadialSubemiitingProjectile : Projectile
{
    public Projectile SubEmissionProjectile;
    public int NumberOfSubemission = 8;

    public override void OnDeath()
    {
        Vector2 defaultDirection = Direction;

        for (int i = 0; i < NumberOfSubemission; i++)
        {
            float angle = 360 / NumberOfSubemission * i;
            Vector2 emissionDirection = RotateVector(defaultDirection, angle);

            Projectile subProjectile = Instantiate(SubEmissionProjectile, transform.position, Quaternion.identity);
            subProjectile.Initialize(emissionDirection, Targets);
        }
    }
}
