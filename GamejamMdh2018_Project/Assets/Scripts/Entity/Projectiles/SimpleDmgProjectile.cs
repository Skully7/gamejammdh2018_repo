﻿using UnityEngine;

public class SimpleDmgProjectile : Projectile
{
    public int Damage = 10;

    public GameObject ImpactEffectPrefab;

    public override void OnHit(IDamageable hitObject)
    {
        hitObject.TakeDamage(Damage);
    }

    public override void OnDeath()
    {
        if (ImpactEffectPrefab != null)
            Instantiate(ImpactEffectPrefab, transform.position, Quaternion.identity);
    }
}
