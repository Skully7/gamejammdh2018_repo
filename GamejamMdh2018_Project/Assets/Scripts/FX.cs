﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tastenhacker.Core.Manager;
using UnityEngine.Rendering.PostProcessing;

public class FX : ManagerModule
{
    [SerializeField] private PostProcessProfile Profile;

    private float _currentVignettePos;
    [SerializeField][Range(0,1)] public float VignetteLevel;

    [SerializeField] private AnimationCurve VignetteSpeed;
    [SerializeField] private AnimationCurve VignetteAnimation;

    public void Update()
    {
        _currentVignettePos = (_currentVignettePos + Time.deltaTime * VignetteSpeed.Evaluate(VignetteLevel)) % 1.0f;
        Profile.GetSetting<Vignette>().intensity.value = VignetteLevel * VignetteAnimation.Evaluate(_currentVignettePos);
    }
    
}
