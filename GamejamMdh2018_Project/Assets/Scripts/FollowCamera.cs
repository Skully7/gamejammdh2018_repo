﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Transform Target;
    public float Distance;

	void Update ()
	{
	    transform.position = Target.transform.position - new Vector3(0, 0, Distance);
	}
}
