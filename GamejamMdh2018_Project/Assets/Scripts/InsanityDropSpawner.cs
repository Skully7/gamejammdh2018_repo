﻿using UnityEngine;

public class InsanityDropSpawner : MonoBehaviour
{
    private static int SmallDropsMin = 1;
    private static int SmallDropsMax = 2;
    private static int LargeDropsMin = 5;
    private static int LargeDropsMax = 8;

    private static InsanityDropSpawner _instance;

    public static InsanityDropSpawner Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<InsanityDropSpawner>();
            return _instance;
        }
    }

    public InsanityDrop SmallDropPrefab;
    public InsanityDrop LargeDropPrefab;

    public Player PlayerObj;


    protected virtual void Start()
    {
        if (PlayerObj == null)
        {
            Debug.Log("[InsanityDropSpawner] Attempt to auto-assign player to the script.");
            PlayerObj = FindObjectOfType<Player>();
        }
    }

    public void Spawn(Vector3 point, int small, int large = 0)
    {
        for(int i = 0; i < small; i++)
        {
            InsanityDrop drop = Instantiate(SmallDropPrefab, point, Quaternion.identity);
            drop.Initialize(PlayerObj.transform, Random.Range(SmallDropsMin, SmallDropsMax));
        }

        for (int i = 0; i < large; i++)
        {
            InsanityDrop drop = Instantiate(LargeDropPrefab, point, Quaternion.identity);
            drop.Initialize(PlayerObj.transform, Random.Range(LargeDropsMin, LargeDropsMax));
        }
    }

    public void Spawn(Vector3 point, Vector2 smallAndLarge)
    {
        Spawn(point, (int)smallAndLarge.x, (int)smallAndLarge.y);
    }

}
