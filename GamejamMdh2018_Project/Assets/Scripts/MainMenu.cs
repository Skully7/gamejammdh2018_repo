﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	public void StartGame()
    {
        Debug.Log("Starting Game");
        Time.timeScale = 1;
		SceneManager.LoadScene(1);
	}
}
