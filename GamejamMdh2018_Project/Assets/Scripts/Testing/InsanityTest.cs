﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsanityTest : MonoBehaviour
{
    [SerializeField] private Animator BrainAnimator;

	void Update ()
	{
	    if (Input.GetKeyDown(KeyCode.I))
	    {
            BrainAnimator.SetTrigger("GoingInsane");
	    }
	    if (Input.GetKeyDown(KeyCode.S))
	    {
	        BrainAnimator.SetTrigger("GetSane");
	    }
	}
}
