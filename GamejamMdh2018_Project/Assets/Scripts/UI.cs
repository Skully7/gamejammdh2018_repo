﻿using UnityEngine;
using Tastenhacker.Core.Manager;
using UnityEngine.UI;

public class UI : ManagerModule
{
    public Animator HeartAnimator;
    public Animator BrainAnimator;

    public Image HealthBar;
    public Image InsanityBar;
    //public Image InsenityBarFill;
}