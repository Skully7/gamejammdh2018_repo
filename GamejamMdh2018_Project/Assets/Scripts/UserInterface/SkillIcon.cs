﻿using UnityEngine;
using UnityEngine.UI;

public class SkillIcon : MonoBehaviour
{
    public Image Icon;
    public GameObject DisabledLayer;
    public Image CooldownImage;
    public GameObject SelectionIndicator;

    public void Initialize(PlayerAbility pAbility, bool isSelected = false)
    {
        Icon.sprite = pAbility.Icon;
        CooldownImage.fillAmount = 0;
        SelectionIndicator.SetActive(isSelected);
    }

    public void UpdateCooldown(float amount)
    {
        CooldownImage.fillAmount = Mathf.Clamp01(amount);
    }

    public void SetSelected(bool state)
    {
        SelectionIndicator.SetActive(state);
    }

    public void SetActiveState(bool state)
    {
        Icon.gameObject.SetActive(state);
        DisabledLayer.gameObject.SetActive(!state);
    }
}
