﻿using System.Collections.Generic;
using Tastenhacker.Core.Manager;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UiControls : MonoBehaviour
{
    public Player PlayerObject;

    public GameObject SkillIconTemplate;

    private UI _ui;

    public Color InsenityBarBurnout = Color.blue;
    private Color _insenityBarDefaultTint;

    private readonly List<SkillIcon> _allSkillIcons = new List<SkillIcon>();


    protected virtual void Start()
    {
        _ui = Manager.Instance.Get<UI>();
        _insenityBarDefaultTint = _ui.InsanityBar.color;

        if (PlayerObject == null)
        {
            PlayerObject = FindObjectOfType<Player>();
            Debug.Log($"[UiControls] Attempt to automatically find player-reference. Success: {PlayerObject != null}");
        }

        InitializeHotbar();
    }

    protected virtual void OnEnable()
    {
        PlayerObject.OnHealthChanged += UpdateHeartbeat;
        PlayerObject.OnInsanityChanged += UpdateInsanitybar;
        PlayerObject.OnCurrentSkillChanged += ChangeSelection;
        PlayerObject.OnInsanityStateChanged += UpdateInsanityState;
    }

    protected virtual void OnDisable()
    {
        PlayerObject.OnHealthChanged -= UpdateHeartbeat;
        PlayerObject.OnInsanityChanged -= UpdateInsanitybar;
        PlayerObject.OnCurrentSkillChanged -= ChangeSelection;
        PlayerObject.OnInsanityStateChanged -= UpdateInsanityState;
    }

    protected virtual void Update()
    {
        UpdateCooldowns();
        UpdateActiveStates();
    }

    private void UpdateHeartbeat()
    {
        if (PlayerObject.Hp <= 0)
        {
            _ui.HeartAnimator.SetFloat("HeartBeatSpeed", 0);
        }
        else
        {
            _ui.HeartAnimator.SetFloat("HeartBeatSpeed", 1 + 2 * ((Player.MaxHp - PlayerObject.Hp) / 100f));
        }

        _ui.HealthBar.fillAmount = 1 - (float) PlayerObject.Hp / Player.MaxHp;
    }

    private void UpdateInsanitybar()
    {
        _ui.InsanityBar.color = PlayerObject.IsInBurnout ? InsenityBarBurnout : _insenityBarDefaultTint;

        float fill = (float)PlayerObject.Insanity / Player.MaxInsanity;
        _ui.InsanityBar.fillAmount = fill;
    }

    private void UpdateInsanityState(bool insane)
    {
        if(insane)
            _ui.BrainAnimator.SetTrigger("GoingInsane");
        else
            _ui.BrainAnimator.SetTrigger("GetSane");
    }


    private void InitializeHotbar()
    {
        for (int i = _allSkillIcons.Count - 1; i >= 0; i--)
        {
            Destroy(_allSkillIcons[i].gameObject);
        }
        _allSkillIcons.Clear();

        for (int i = 0; i < PlayerObject.CastInfos.Length; i++)
        {
            PlayerAbility pAbility = PlayerObject.CastInfos[i].AbilityObject;

            GameObject newIcon = Instantiate(SkillIconTemplate, SkillIconTemplate.transform.parent, true);
            SkillIcon newIconSkill = newIcon.GetComponent<SkillIcon>();

            newIconSkill.gameObject.SetActive(true);
            newIconSkill.Initialize(pAbility, i == PlayerObject.CurrentSelectedAbility);

            _allSkillIcons.Add(newIconSkill);
        }
    }

    private void ChangeSelection()
    {
        for (int i = 0; i < _allSkillIcons.Count; i++)
        {
            _allSkillIcons[i].SetSelected(i == PlayerObject.CurrentSelectedAbility);
        }
    }

    private void UpdateCooldowns()
    {
        for(int i = 0 ; i < _allSkillIcons.Count; i++)
        {
            _allSkillIcons[i].UpdateCooldown(PlayerObject.GetCooldownAmount(i));
        }
    }

    private void UpdateActiveStates()
    {
        for (int i = 0; i < _allSkillIcons.Count; i++)
        {
            _allSkillIcons[i].SetActiveState(PlayerObject.GetAbilityState(i));
        }
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
