﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedBlinkDmgEffect : MonoBehaviour
{
    public float EffectDuration;
    public AnimationCurve EffectCurve;

    private Coroutine _effectRoutine;

    private SpriteRenderer[] _allObjRenderer;
    private float _timer;

    private readonly Dictionary<SpriteRenderer, Color> _defaultColors = new Dictionary<SpriteRenderer, Color>();

    protected virtual void Start()
    {
        _allObjRenderer = GetComponentsInChildren<SpriteRenderer>();

        if(_allObjRenderer.Length <= 0)
            Debug.LogWarning($"[RedBlinkEffect] Could not find any SpriteRenderer to manipulate on '{gameObject.name}' or in children!");

        foreach (SpriteRenderer rend in _allObjRenderer)
        {
            _defaultColors.Add(rend, rend.color);
        }
    }

    public void Play()
    {
        if(_effectRoutine != null)
            StopCoroutine(_effectRoutine);
        _effectRoutine = StartCoroutine(Effect());
    }


    private IEnumerator Effect()
    {
        _timer = 0;

        while (_timer <= EffectDuration)
        {
            float amount = EffectCurve.Evaluate(_timer / EffectDuration);

            foreach (SpriteRenderer rend in _allObjRenderer)
            {
                rend.color = Color.Lerp(_defaultColors[rend], Color.red, amount);
            }

            _timer += Time.deltaTime;
            yield return null;
        }

        foreach (SpriteRenderer rend in _allObjRenderer)
        {
            rend.color = _defaultColors[rend];
        }

        _effectRoutine = null;
    }
}
