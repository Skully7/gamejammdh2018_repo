﻿using System;
using UnityEngine;

public class RotateInDirection : MonoBehaviour
{
    public Rigidbody2D Body;
    [SerializeField, Range(0,360)] private float _rotationOffset;

    protected virtual void Update()
    {
        if(Math.Abs(Body.velocity.magnitude) < Mathf.Epsilon)
            return;

        float angle = Vector2.SignedAngle(Vector2.left, Body.velocity);
        transform.rotation = Quaternion.Euler(0, 0, angle + _rotationOffset);
    }
}
