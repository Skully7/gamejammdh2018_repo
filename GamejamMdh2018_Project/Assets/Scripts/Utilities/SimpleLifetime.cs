﻿using System.Collections;
using UnityEngine;

public class SimpleLifetime : MonoBehaviour
{
    public bool AutostartTimer = true;
    public float Lifetime = 1f;

    private WaitForSeconds _wait;


    protected virtual void Awake()
    {
        _wait = new WaitForSeconds(Lifetime);
    }

	protected virtual void Start ()
	{
	    if(AutostartTimer)
            StartTimer();
	}


    public void StartTimer()
    {
        StartCoroutine(RemoveAfterLifetime());
    }

    private IEnumerator RemoveAfterLifetime()
    {
        yield return _wait;
        Destroy(gameObject);
    }
}
