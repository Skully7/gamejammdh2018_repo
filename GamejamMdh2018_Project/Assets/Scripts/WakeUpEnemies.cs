﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WakeUpEnemies : MonoBehaviour {

	// Use this for initialization
	void Start () {
        foreach (Transform t in transform)
            t.gameObject.SetActive(true);
	}
}
